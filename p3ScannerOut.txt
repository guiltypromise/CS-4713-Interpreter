primClassif subClassif   tokenStr
  1 print("p3Input.txt");
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       p3Input.txt
                                    
SEPARATOR   -            )
  2 Int i;
SEPARATOR   -            ;
CONTROL     DECLARE      Int
OPERAND     IDENTFIER    i
  3 Float pi;
SEPARATOR   -            ;
CONTROL     DECLARE      Float
OPERAND     IDENTFIER    pi
  4 String day;
SEPARATOR   -            ;
CONTROL     DECLARE      String
OPERAND     IDENTFIER    day
  5    day 
SEPARATOR   -            ;
  6    = 
OPERAND     IDENTFIER    day
  7    "Sunday";
OPERATOR    -            =
OPERAND     STRING       Sunday
                               
  8 String name;
SEPARATOR   -            ;
CONTROL     DECLARE      String
OPERAND     IDENTFIER    name
  9 name = "Anita Goodgrade";
SEPARATOR   -            ;
OPERAND     IDENTFIER    name
OPERATOR    -            =
OPERAND     STRING       Anita Goodgrade
                                        
 10 String weird;
SEPARATOR   -            ;
CONTROL     DECLARE      String
OPERAND     IDENTFIER    weird
 11 weird = "\tTX\tTexas\n";
SEPARATOR   -            ;
OPERAND     IDENTFIER    weird
OPERATOR    -            =
OPERAND     STRING       . TX. Texas. 
                         09  09     0A
 12 String loc;
SEPARATOR   -            ;
CONTROL     DECLARE      String
OPERAND     IDENTFIER    loc
 13 loc = "TX";
SEPARATOR   -            ;
OPERAND     IDENTFIER    loc
OPERATOR    -            =
OPERAND     STRING       TX
                           
 14 
 15 // numeric stuff
 16 print(">>> Checking datatype results based on left operand");
SEPARATOR   -            ;
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       >>> Checking datatype results based on left operand
                                                                            
SEPARATOR   -            )
 17 pi = 3.14;
SEPARATOR   -            ;
OPERAND     IDENTFIER    pi
OPERATOR    -            =
OPERAND     FLOAT        3.14
 18 Float area;
SEPARATOR   -            ;
CONTROL     DECLARE      Float
OPERAND     IDENTFIER    area
 19 Float radius2;
SEPARATOR   -            ;
CONTROL     DECLARE      Float
OPERAND     IDENTFIER    radius2
 20 Int radius;
SEPARATOR   -            ;
CONTROL     DECLARE      Int
OPERAND     IDENTFIER    radius
 21 radius = 8;
SEPARATOR   -            ;
OPERAND     IDENTFIER    radius
OPERATOR    -            =
OPERAND     INTEGER      8
 22 radius2 = radius ^ 2;  // square the radius
SEPARATOR   -            ;
OPERAND     IDENTFIER    radius2
OPERATOR    -            =
OPERAND     IDENTFIER    radius
OPERATOR    -            ^
OPERAND     INTEGER      2
 23 print("\t1. area all float");
SEPARATOR   -            ;
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       . 1. area all float
                         09                 
SEPARATOR   -            )
 24 area = pi * radius2;
SEPARATOR   -            ;
OPERAND     IDENTFIER    area
OPERATOR    -            =
OPERAND     IDENTFIER    pi
OPERATOR    -            *
OPERAND     IDENTFIER    radius2
 25 print("\tradius="
SEPARATOR   -            ;
FUNCTION    BUILTIN      print
SEPARATOR   -            (
 26       ,  radius
OPERAND     STRING       . radius=
                         09       
SEPARATOR   -            ,
 27       ,  "radius2="
OPERAND     IDENTFIER    radius
SEPARATOR   -            ,
 28       ,  radius2
OPERAND     STRING       radius2=
                                 
SEPARATOR   -            ,
 29       ,  "area="
OPERAND     IDENTFIER    radius2
SEPARATOR   -            ,
 30       ,  area);
OPERAND     STRING       area=
                              
SEPARATOR   -            ,
OPERAND     IDENTFIER    area
SEPARATOR   -            )
 31 Int irad2;
SEPARATOR   -            ;
CONTROL     DECLARE      Int
OPERAND     IDENTFIER    irad2
 32 irad2 = radius ^ 2;
SEPARATOR   -            ;
OPERAND     IDENTFIER    irad2
OPERATOR    -            =
OPERAND     IDENTFIER    radius
OPERATOR    -            ^
OPERAND     INTEGER      2
 33 print("\t2. area using int radius as left operand");
SEPARATOR   -            ;
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       . 2. area using int radius as left operand
                         09                                        
SEPARATOR   -            )
 34 area = irad2 * pi;
SEPARATOR   -            ;
OPERAND     IDENTFIER    area
OPERATOR    -            =
OPERAND     IDENTFIER    irad2
OPERATOR    -            *
OPERAND     IDENTFIER    pi
 35 print("\tradius=", radius, "irad2=", irad2, "area=", area);
SEPARATOR   -            ;
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       . radius=
                         09       
SEPARATOR   -            ,
OPERAND     IDENTFIER    radius
SEPARATOR   -            ,
OPERAND     STRING       irad2=
                               
SEPARATOR   -            ,
OPERAND     IDENTFIER    irad2
SEPARATOR   -            ,
OPERAND     STRING       area=
                              
SEPARATOR   -            ,
OPERAND     IDENTFIER    area
SEPARATOR   -            )
 36 Float circum;
SEPARATOR   -            ;
CONTROL     DECLARE      Float
OPERAND     IDENTFIER    circum
 37 Float twoPi;
SEPARATOR   -            ;
CONTROL     DECLARE      Float
OPERAND     IDENTFIER    twoPi
 38 print("\t3. circumference using twoPi as float * int");
SEPARATOR   -            ;
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       . 3. circumference using twoPi as float * int
                         09                                           
SEPARATOR   -            )
 39 twoPi = pi * 2;
SEPARATOR   -            ;
OPERAND     IDENTFIER    twoPi
OPERATOR    -            =
OPERAND     IDENTFIER    pi
OPERATOR    -            *
OPERAND     INTEGER      2
 40 circum = twoPi * radius;
SEPARATOR   -            ;
OPERAND     IDENTFIER    circum
OPERATOR    -            =
OPERAND     IDENTFIER    twoPi
OPERATOR    -            *
OPERAND     IDENTFIER    radius
 41 print("\tradius=", radius, "twoPi=", twoPi, "circum=", circum);
SEPARATOR   -            ;
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       . radius=
                         09       
SEPARATOR   -            ,
OPERAND     IDENTFIER    radius
SEPARATOR   -            ,
OPERAND     STRING       twoPi=
                               
SEPARATOR   -            ,
OPERAND     IDENTFIER    twoPi
SEPARATOR   -            ,
OPERAND     STRING       circum=
                                
SEPARATOR   -            ,
OPERAND     IDENTFIER    circum
SEPARATOR   -            )
 42 print("\t4. circumference using twoPi as int * float");
SEPARATOR   -            ;
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       . 4. circumference using twoPi as int * float
                         09                                           
SEPARATOR   -            )
 43 twoPi = 2 * pi;
SEPARATOR   -            ;
OPERAND     IDENTFIER    twoPi
OPERATOR    -            =
OPERAND     INTEGER      2
OPERATOR    -            *
OPERAND     IDENTFIER    pi
 44 circum = twoPi * radius;
SEPARATOR   -            ;
OPERAND     IDENTFIER    circum
OPERATOR    -            =
OPERAND     IDENTFIER    twoPi
OPERATOR    -            *
OPERAND     IDENTFIER    radius
 45 print("\tradius=", radius, "twoPi=", twoPi, "circum=", circum);
SEPARATOR   -            ;
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       . radius=
                         09       
SEPARATOR   -            ,
OPERAND     IDENTFIER    radius
SEPARATOR   -            ,
OPERAND     STRING       twoPi=
                               
SEPARATOR   -            ,
OPERAND     IDENTFIER    twoPi
SEPARATOR   -            ,
OPERAND     STRING       circum=
                                
SEPARATOR   -            ,
OPERAND     IDENTFIER    circum
SEPARATOR   -            )
 46 
 47 // Minus stuff
 48 
 49 Int A;
SEPARATOR   -            ;
CONTROL     DECLARE      Int
OPERAND     IDENTFIER    A
 50 Int B;
SEPARATOR   -            ;
CONTROL     DECLARE      Int
OPERAND     IDENTFIER    B
 51 Float D;
SEPARATOR   -            ;
CONTROL     DECLARE      Float
OPERAND     IDENTFIER    D
 52 A = 10;
SEPARATOR   -            ;
OPERAND     IDENTFIER    A
OPERATOR    -            =
OPERAND     INTEGER      10
 53 B = 6;
SEPARATOR   -            ;
OPERAND     IDENTFIER    B
OPERATOR    -            =
OPERAND     INTEGER      6
 54 D = 25;
SEPARATOR   -            ;
OPERAND     IDENTFIER    D
OPERATOR    -            =
OPERAND     INTEGER      25
 55 print(">> Minus test, A=", A, "B=", B, "D=", D);
SEPARATOR   -            ;
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       >> Minus test, A=
                                          
SEPARATOR   -            ,
OPERAND     IDENTFIER    A
SEPARATOR   -            ,
OPERAND     STRING       B=
                           
SEPARATOR   -            ,
OPERAND     IDENTFIER    B
SEPARATOR   -            ,
OPERAND     STRING       D=
                           
SEPARATOR   -            ,
OPERAND     IDENTFIER    D
SEPARATOR   -            )
 56 print("D-A=", D-A);
SEPARATOR   -            ;
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       D-A=
                             
SEPARATOR   -            ,
OPERAND     IDENTFIER    D
OPERATOR    -            -
OPERAND     IDENTFIER    A
SEPARATOR   -            )
 57 print("-A=", -A);
SEPARATOR   -            ;
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       -A=
                            
SEPARATOR   -            ,
OPERATOR    -            -
OPERAND     IDENTFIER    A
SEPARATOR   -            )
 58 print("-D=", -D);
SEPARATOR   -            ;
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       -D=
                            
SEPARATOR   -            ,
OPERATOR    -            -
OPERAND     IDENTFIER    D
SEPARATOR   -            )
 59 A = -B;
SEPARATOR   -            ;
OPERAND     IDENTFIER    A
OPERATOR    -            =
OPERATOR    -            -
OPERAND     IDENTFIER    B
 60 print("A=-B =", A);
SEPARATOR   -            ;
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       A=-B =
                               
SEPARATOR   -            ,
OPERAND     IDENTFIER    A
SEPARATOR   -            )
 61 
 62 // Simple if
 63 i = 0;
SEPARATOR   -            ;
OPERAND     IDENTFIER    i
OPERATOR    -            =
OPERAND     INTEGER      0
 64 print(">>> First If");
SEPARATOR   -            ;
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       >>> First If
                                     
SEPARATOR   -            )
 65 if loc == "TX": // check for TX
SEPARATOR   -            ;
CONTROL     FLOW         if
OPERAND     IDENTFIER    loc
OPERATOR    -            ==
OPERAND     STRING       TX
                           
 66     // It is TX
 67     print("\tloc is", loc);
SEPARATOR   -            :
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       . loc is
                         09      
SEPARATOR   -            ,
OPERAND     IDENTFIER    loc
SEPARATOR   -            )
 68     // it is ok to have this statement on multiple lines.  
 69     // That is why we have a semicolon
 70     i 
SEPARATOR   -            ;
 71        = 
OPERAND     IDENTFIER    i
 72           i 
OPERATOR    -            =
 73           + 1;
OPERAND     IDENTFIER    i
OPERATOR    -            +
OPERAND     INTEGER      1
 74 endif;
SEPARATOR   -            ;
CONTROL     END          endif
 75 print("\tDone with first if");
SEPARATOR   -            ;
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       . Done with first if
                         09                  
SEPARATOR   -            )
 76 
 77 print(">>> Second If");
SEPARATOR   -            ;
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       >>> Second If
                                      
SEPARATOR   -            )
 78 // if then else
 79 if i >= 5:
SEPARATOR   -            ;
CONTROL     FLOW         if
OPERAND     IDENTFIER    i
OPERATOR    -            >=
OPERAND     INTEGER      5
 80     print("\t2nd if true, i >= 5, i =", i);
SEPARATOR   -            :
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       . 2nd if true, i >= 5, i =
                         09                        
SEPARATOR   -            ,
OPERAND     IDENTFIER    i
SEPARATOR   -            )
 81 else:
SEPARATOR   -            ;
CONTROL     END          else
 82     print("\t2nd if false, i >= 5, i =", i);
SEPARATOR   -            :
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       . 2nd if false, i >= 5, i =
                         09                         
SEPARATOR   -            ,
OPERAND     IDENTFIER    i
SEPARATOR   -            )
 83 endif;
SEPARATOR   -            ;
CONTROL     END          endif
 84 // while and if 
 85 print(">>> First while i < 5");    
SEPARATOR   -            ;
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       >>> First while i < 5
                                              
SEPARATOR   -            )
 86 i = 1;
SEPARATOR   -            ;
OPERAND     IDENTFIER    i
OPERATOR    -            =
OPERAND     INTEGER      1
 87 while i < 5:
SEPARATOR   -            ;
CONTROL     FLOW         while
OPERAND     IDENTFIER    i
OPERATOR    -            <
OPERAND     INTEGER      5
 88     print("\ttop of while, i=", i);
SEPARATOR   -            :
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       . top of while, i=
                         09                
SEPARATOR   -            ,
OPERAND     IDENTFIER    i
SEPARATOR   -            )
 89     if i < 3:
SEPARATOR   -            ;
CONTROL     FLOW         if
OPERAND     IDENTFIER    i
OPERATOR    -            <
OPERAND     INTEGER      3
 90         print("\twhile first if true i<3, i=", i);
SEPARATOR   -            :
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       . while first if true i<3, i=
                         09                           
SEPARATOR   -            ,
OPERAND     IDENTFIER    i
SEPARATOR   -            )
 91         i = i + 2;
SEPARATOR   -            ;
OPERAND     IDENTFIER    i
OPERATOR    -            =
OPERAND     IDENTFIER    i
OPERATOR    -            +
OPERAND     INTEGER      2
 92         if loc != "TX":
SEPARATOR   -            ;
CONTROL     FLOW         if
OPERAND     IDENTFIER    loc
OPERATOR    -            !=
OPERAND     STRING       TX
                           
 93              print("\t\twhile inner if true ... wrong!");
SEPARATOR   -            :
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       . . while inner if true ... wrong!
                         0909                              
SEPARATOR   -            )
 94         else:
SEPARATOR   -            ;
CONTROL     END          else
 95              print('\t\twhile inner if false, loc!=\'TX\', loc is', loc);
SEPARATOR   -            :
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       . . while inner if false, loc!='TX', loc is
                         0909                                       
SEPARATOR   -            ,
OPERAND     IDENTFIER    loc
SEPARATOR   -            )
 96         endif;
SEPARATOR   -            ;
CONTROL     END          endif
 97     else:
SEPARATOR   -            ;
CONTROL     END          else
 98         print("\twhile first if false i<3, i=", i);
SEPARATOR   -            :
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       . while first if false i<3, i=
                         09                            
SEPARATOR   -            ,
OPERAND     IDENTFIER    i
SEPARATOR   -            )
 99         if loc=="TX":
SEPARATOR   -            ;
CONTROL     FLOW         if
OPERAND     IDENTFIER    loc
OPERATOR    -            ==
OPERAND     STRING       TX
                           
100              print("\t\twhile 2nd inner if true loc=='TX', loc is", loc);
SEPARATOR   -            :
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       . . while 2nd inner if true loc=='TX', loc is
                         0909                                         
SEPARATOR   -            ,
OPERAND     IDENTFIER    loc
SEPARATOR   -            )
101              i = i + 1;
SEPARATOR   -            ;
OPERAND     IDENTFIER    i
OPERATOR    -            =
OPERAND     IDENTFIER    i
OPERATOR    -            +
OPERAND     INTEGER      1
102         else:
SEPARATOR   -            ;
CONTROL     END          else
103              print("\t\twhile 2nd inner if false ... wrong");
SEPARATOR   -            :
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       . . while 2nd inner if false ... wrong
                         0909                                  
SEPARATOR   -            )
104              i = i + 10;
SEPARATOR   -            ;
OPERAND     IDENTFIER    i
OPERATOR    -            =
OPERAND     IDENTFIER    i
OPERATOR    -            +
OPERAND     INTEGER      10
105         endif;
SEPARATOR   -            ;
CONTROL     END          endif
106         print("\twhile first if false after inner if");
SEPARATOR   -            ;
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       . while first if false after inner if
                         09                                   
SEPARATOR   -            )
107     endif;
SEPARATOR   -            ;
CONTROL     END          endif
108     print("\tbottom of while, i=", i);
SEPARATOR   -            ;
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       . bottom of while, i=
                         09                   
SEPARATOR   -            ,
OPERAND     IDENTFIER    i
SEPARATOR   -            )
109 endwhile;
SEPARATOR   -            ;
CONTROL     END          endwhile
110 print("\tAfter first while");
SEPARATOR   -            ;
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       . After first while
                         09                 
SEPARATOR   -            )
111 
112 // nested while loops
113 print(">>> Second while i < 5 ... with nested while");
SEPARATOR   -            ;
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       >>> Second while i < 5 ... with nested while
                                                                     
SEPARATOR   -            )
114 i = 1;
SEPARATOR   -            ;
OPERAND     IDENTFIER    i
OPERATOR    -            =
OPERAND     INTEGER      1
115 Int j;
SEPARATOR   -            ;
CONTROL     DECLARE      Int
OPERAND     IDENTFIER    j
116 Bool bFlag;
SEPARATOR   -            ;
CONTROL     DECLARE      Bool
OPERAND     IDENTFIER    bFlag
117 bFlag = T;
SEPARATOR   -            ;
OPERAND     IDENTFIER    bFlag
OPERATOR    -            =
OPERAND     BOOLEAN      T
118 while i < 5:
SEPARATOR   -            ;
CONTROL     FLOW         while
OPERAND     IDENTFIER    i
OPERATOR    -            <
OPERAND     INTEGER      5
119     j = i;
SEPARATOR   -            :
OPERAND     IDENTFIER    j
OPERATOR    -            =
OPERAND     IDENTFIER    i
120     print("\ttop of while, i=", i, "j=", j);
SEPARATOR   -            ;
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       . top of while, i=
                         09                
SEPARATOR   -            ,
OPERAND     IDENTFIER    i
SEPARATOR   -            ,
OPERAND     STRING       j=
                           
SEPARATOR   -            ,
OPERAND     IDENTFIER    j
SEPARATOR   -            )
121     print("\t>>> Inner while j <= 3");
SEPARATOR   -            ;
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       . >>> Inner while j <= 3
                         09                      
SEPARATOR   -            )
122     while  j <= 3:
SEPARATOR   -            ;
CONTROL     FLOW         while
OPERAND     IDENTFIER    j
OPERATOR    -            <=
OPERAND     INTEGER      3
123         print("\t\tinner while, j=", j);
SEPARATOR   -            :
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       . . inner while, j=
                         0909               
SEPARATOR   -            ,
OPERAND     IDENTFIER    j
SEPARATOR   -            )
124         j = j + 1;
SEPARATOR   -            ;
OPERAND     IDENTFIER    j
OPERATOR    -            =
OPERAND     IDENTFIER    j
OPERATOR    -            +
OPERAND     INTEGER      1
125     endwhile;
SEPARATOR   -            ;
CONTROL     END          endwhile
126     bFlag = F;
SEPARATOR   -            ;
OPERAND     IDENTFIER    bFlag
OPERATOR    -            =
OPERAND     BOOLEAN      F
127     print("\tbottom of while, i=", i, "j=", j);
SEPARATOR   -            ;
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       . bottom of while, i=
                         09                   
SEPARATOR   -            ,
OPERAND     IDENTFIER    i
SEPARATOR   -            ,
OPERAND     STRING       j=
                           
SEPARATOR   -            ,
OPERAND     IDENTFIER    j
SEPARATOR   -            )
128     i = i + 1;
SEPARATOR   -            ;
OPERAND     IDENTFIER    i
OPERATOR    -            =
OPERAND     IDENTFIER    i
OPERATOR    -            +
OPERAND     INTEGER      1
129 endwhile;
SEPARATOR   -            ;
CONTROL     END          endwhile
130 print("\tAfter outer while");
SEPARATOR   -            ;
FUNCTION    BUILTIN      print
SEPARATOR   -            (
OPERAND     STRING       . After outer while
                         09                 
SEPARATOR   -            )
131         
