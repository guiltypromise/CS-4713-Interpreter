Testing i += 2, where i = 3
i should be 5; i == 5 
Testing i -= 4, where i = 8
i should be 4; i == 4 
Testing i += (- 2 1), where i = 7
i should be 8; i == 8 
Testing error case

File:   Plusminusequaltest.txt
Line 15: Expected = after +
	at parser.Parser.error(Parser.java:2284)
	at parser.Parser.assignmentStmt(Parser.java:1361)
	at parser.Parser.statements(Parser.java:89)
	at havabol.HavaBol.main(HavaBol.java:48)