# CS 4713 Interpreter Project

## Team Members: Chris Cordova, Jacob Mazur, Jason Martin

## Team Name: The Best Prefix Guys

## Project Status: Tentative Submission as PGM 5, due Thurs 4/27/2017

### Tasks (Flexible Requirements)
- [ ] Input Files (-150 to 30 pts depending on quality)
	- [ ] Test cases
	- [ ] Error cases
- [ ] Requirements (190 pts)
	- [ ] Date Data Type (10 pts)
	- [ ] Unbound Arrays (5 pts)
	- [X] Additional Numeric Assignment Operators += and -= (5 pts)
	- [X] IN and NOTIN functions (10 pts)
	- [ ] Slices (30 pts)
	- [X] Tokenizing a string (10 pts)
	- [ ] Break and Continue (10 pts)
	- [X] select when default endselect (10 pts)



	
	

