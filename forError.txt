// arrays, element assignment, for loops
// do simple variables still work?
print("This file is for testing for loop error checking");
Int iSimp = 50;
print ("iSimp=" iSimp);

String forLoop = "for";
String blank = "          ";

print("Array C with elements printed using constant subscripts");
Int iCM[10]= 10, 20, 30, 40, 50, 60;
//iCM[1] = 40;

print(" Check SPACES on 'blank' " (SPACES blank));


print("\tC " (array iCM 0) " " (array iCM 1) " " (array iCM 2));

print("Array B with expressions for subscripts");
Int iBM[] = 10, 20, 30;
iSimp = 0;
print("\tB " (array iBM iSimp) " " (* 10 (array iBM (+ iSimp 1))) " "  (array iBM (+ 1 (+ iSimp 1))));

print("Array B with elements replaced");
iSimp=1;
iBM[iSimp] = 100;
//iBM[iSimp+1] = 200;
iBM[ (+ iSimp 1 ) ] = 200;
print("\tB.2 " (array iBM 0) " " (array iBM iSimp) " " (array iBM (+ iSimp 1)));

print("Array F with arrays of arrays");
Int iFM[] = 10, 20, 30, 40, 50, 0, 1, 2, 3,4,5;
iSimp = 1;
print("\tF " 
		(array iFM (- iSimp 1)) " "
		(array iFM (array iFM 6)) " "
		(array iFM (/ (- (+ iSimp 2) 1) 10)) " "
       	(array iFM (array iFM (/ (- (+ iSimp 2) 1) 10 ) ) ) " "
	    (array iFM (* 2 4)) " "
	    (array iFM (- (+ 2 (array iFM (* 2 4))) 1) )
      );
// BM[iBM[0]-8]);
print("iAM originally does not have any elements");
Int iAM[10];
print("iAm number of elements is " (ELEM iAM));
iSimp = 1;
iAM[iSimp] = 100; // not an error
print("\tA " (array iAM 1));

// for loops -------------------------------------------------
print("for loop for val in iCM");
for iVal in iCM:
    print("\t" iVal);
endfor;
Int i;
print("counting for loop");
for i = 0 to (ELEM iCM):
//    print("\t", iCM[i]);
     print("\t" (array iCM i));
endfor;

print("counting for loop by 2, implicit variable k");
for k = 0 to (ELEM iCM) by 2:
    print("\t" (array iCM k));
endfor;

print("Unsorted array");
Int iDM[20] = 60,30,20,10,5,50,70,25,35,45,15;
Int iTemp;
for iTemp in iDM:
    print("\t" iTemp);
endfor;

print("Nested for loops, sorting the array");
Bool bChange;
Int j;
for i=0 to (- (ELEM iDM) 1):
    bChange = F;
    for j=0 to (- (- (ELEM iDM) i) 1) by 1:
        if (< (array iDM (+ j 1))  (array iDM j)):
           iTemp = (array iDM (+ j 1));
           iDM[(+ j 1)] = (array iDM j);
           iDM [j] = iTemp;
           bChange = T;
        endif;
    endfor;
    if (not bChange):
        print("\tready to leave, i=" i);
        // break;
    endif;
endfor; 
print("sorted array");
for  to (ELEM iDM):
    print("\t" (array iDM i));
endfor;

// Assign a value to the entire array
print("Scalar assignment to an array");
String strM[10];
strM = "oh yes";
for oneStr in strM:
    print("\t" oneStr);
endfor;
// Copy the array
Int iCopyAM[5];
iCopyAM = iDM;  // copy the values to the array stop when we have filled it.
print("copy of the sorted array, but with just 5 elements");
for iTemp in iCopyAM:
    print("\t" iTemp);
endfor;

// Copy the array
Int iCopyBM[30];
iCopyBM = iDM;  // copy the values to the array stop when we run out of values
print("copy of the sorted array, but with just 11 elements not 30");
for iTemp in iCopyBM:
    print("\t" iTemp);
endfor;