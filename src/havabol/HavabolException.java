package havabol;

public class HavabolException extends Exception {

	public HavabolException( String msg ) {
		super(msg);
	}
}
