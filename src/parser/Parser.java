package parser;


import java.util.ArrayList;

import scanner.Scanner;
import symboltable.STEntry;
import symboltable.STIdentifier;
import symboltable.SymbolTable;
import utility.Date;
import utility.Numeric;
import utility.StorageManager;
import utility.Utility;
import token.Token;

public class Parser 
{
	Scanner scan;			// a reference to the Scanner
	SymbolTable st;			// a reference to the SymbolTable
	private ResultValue resOp; // needs to be not an object ( tentative ResultValue operand )
	private int iLineNr;
	private String sourceFileNm;
	private boolean bExec; // ignore/execute flag
	private StorageManager storageMgr;
	private Token savedToken;

	
	private int parenCount;
	
	// Constants for symbol table entry structure types
	private static final int PRIMITIVE = 0;		// String, Int, Bool, ...
	private static final int FIXED_ARRAY = 1;   // Int[], Float[]
	private static final int UNBOUNDED_ARRAY = 2; // idk
	
	// Constants for symbol tabke parameter types
	private static final int NOT_PARM = 0;
	private static final int BY_REF = 1;
	private static final int BY_VAL = 2;
	
	// cheat and use static for array declared flag, arraySize, and/or ArrayList of ResultValues?
	private static int declaredArraySize = 0;
	/**
	 * Constructs a new Parser object with a reference to the SymbolTable
	 * and the Scanner.
	 * @param SymbolTable st A reference to the SymbolTable
	 * @param Scanner scan A reference to the Scanner
	 */
	public Parser(Scanner scanner, SymbolTable symTable)
	{
		this.scan = scanner;
		this.st = symTable;
		this.sourceFileNm = scan.sourceFileNm;
		bExec = true;
		storageMgr = new StorageManager();

		savedToken = null;
		parenCount = 0;
	}
	
	/**
	 * Uses Scanner to grab one token at a time, interpreting the code
	 * and outputting via stdout
	 */
	public ResultValue statements( boolean bExec )
	{
		//this.bExec = bExec;
		ResultValue result = null;
        try 
        {
        	//Get the next token from the Scanner
			while (! scan.getNext().isEmpty() )
			{
				//System.out.printf("\t\tDEBUG: Inside statements(): position is %s\n", scan.currentToken.iSourceLineNr+1 );
				//scan.currentToken.printToken();
				//scan.nextToken.printToken();
			    if ( scan.currentToken.tokenStr.equals("print") ) // the beginning of a statement is "print"
			    {
			    	//System.out.println("FOUND PRINT: " + scan.currentToken.tokenStr);

			    	result = printStmt(bExec);
			    	parenCount = 0;
			    }
			    else if (scan.currentToken.subClassif == Token.DECLARE) // declare statement, starts with Int, String, ...
			    {
			    	result = declareStmt(bExec);
			    }
			    else if ( scan.currentToken.primClassif == Token.IDENTIFIER) // possible assignment operation
			    {
			    	//scan.currentToken.printToken();
			    	result = assignmentStmt( bExec );
			    }
			    else if ( scan.currentToken.tokenStr.equals("debug"))
			    {
			    	result = debugStmt( bExec );
			    }
			    else if ( scan.currentToken.tokenStr.equals("if"))
			    {
			    	result = ifStmt( bExec );
			    }    
			    else if ( scan.currentToken.tokenStr.equals("else"))
			    {
			    	
			    	result = new ResultValue();
			    	result.terminatingStr = "else";
			    	return result;
			    	
			    }
			    else if ( scan.currentToken.tokenStr.equals("endif"))
			    {
			    	result = new ResultValue();
			    	result.terminatingStr = "endif";
			    	//bExec = true;
			    	return result;
			    	//result = ifStmt(true);
			    }
			    else if (scan.currentToken.tokenStr.equals("while")){
			    	result = whileStmt(bExec);
			    }			  
			    else if (scan.currentToken.tokenStr.equals("for")){
			    	result = forStmt(bExec);
			    }
			    else if (scan.currentToken.tokenStr.equals("endfor")){
					result = new ResultValue();
			    	result.terminatingStr = "endfor";
			    	return result;
			    }
			    else if (scan.currentToken.tokenStr.equals("endwhile")){
					result = new ResultValue();
			    	result.terminatingStr = "endwhile";
			    	return result;
			    }
			    // Past here are things for select
			    else if (scan.currentToken.tokenStr.equals("select")){ // like a switch
			    	//scan.currentToken.printToken();

			    	result = selectStmt(bExec);
			    }
			    else if (scan.currentToken.tokenStr.equals("endselect")){
			    	result = new ResultValue();
			    	result.terminatingStr = "endselect";
			    	return result;
			    }
			    else if (scan.currentToken.tokenStr.equals("when")){
			    	result = new ResultValue();
			    	result.terminatingStr = "when";
			    	//result = selectStmt(false);
			    	return result;
			    }
			    else if (scan.currentToken.tokenStr.equals("default")){
			    	result = new ResultValue();
			    	result.terminatingStr = "default";
			    	return result;
			    }
			    /*
			    // Need a way to get these to the end position of the loop they are in
			    else if (scan.currentToken.tokenStr.equals("break")){
			    	// todo?
			    	result = new ResultValue();
			    	//while(!result.terminatingStr.equals("endfor") || !result.terminatingStr.equals("endwhile"))
			    		//result = statements(false);
			    	result.terminatingStr = "break";
			    	//return result;
			    }
			    else if (scan.currentToken.tokenStr.equals("continue")){
			    	// todo
			    	result = new ResultValue();
			    	//while(!result.terminatingStr.equals("endfor") || !result.terminatingStr.equals("endwhile"))
			    		//result = statements(false);
			    	result.terminatingStr = "continue";
			    	//return result;
			    }
			    */
			}
		}
        catch (Exception e) 
        {
			e.printStackTrace();
			System.exit(-1);
		}
        return result;
	}
	
	/**
	 * Basically a switch statement. 
	 * Values can be Int, String, or Date. Can have nested select statements
	 * Assumption: Current token is "select"
	 */
	ResultValue selectStmt(boolean bExec) throws Exception{
		ResultValue compareValue; // the variable to be checked in when statements
		ResultValue currentValue; // the current variable in the value list to compare
		ResultValue resCond = new ResultValue(); // the condition for the switch construct
		ResultValue resTrueStmts = new ResultValue();
		ResultValue resFalseStmts = new ResultValue();
		int selectLineNum = scan.iSourceLineNr+1;
		
		boolean compareCheck = false; // The result of a check in a when statement
		boolean whenCheck = false;
		boolean defaultCheck = false;
		
		
		if(bExec) // if we are allowed to execute the select operation
		{
			//scan.getNext(); // get rid of select
			
			// possibly error check for an operand, identifier type here
			
			//compareValue = storageMgr.getVariableValue(this, scan.currentToken.tokenStr); // should be the variable
			resCond = preExpr();

			scan.getNext(); // go past variable
			
			if( !scan.currentToken.tokenStr.equals(":"))
			{
				error("Expected \':\' terminator for \'select\'");
			}
			
			scan.getNext(); // eat the :

			while(!scan.currentToken.tokenStr.equals("endselect"))
			{
				//scan.currentToken.printToken();
				switch( scan.currentToken.tokenStr )
				{
					case "when":
						int whenLine = scan.iSourceLineNr+1;
						//scan.getNext(); // eat up the 'when'
						int oldLine = scan.iSourceLineNr+1;

						while( !scan.currentToken.tokenStr.equals(":") )
						{

							// lookahead if we have an operand thats a value or expr
							if( scan.nextToken.primClassif == Token.OPERAND )
							{
								//System.out.println(" >>>>> INSIDE selectStmt()");
								//scan.nextToken.printToken();
	
								currentValue = preExpr();
								//System.out.println(" >>>>> INSIDE selectStmt(): valueList = " + currentValue.value);

								
								if( currentValue.value.equals(resCond.value) )
								{
									whenCheck = true;

									compareCheck = true;
									//int oldLine = scan.iSourceLineNr+1;
									//System.out.println("\t\t\t\t\t Current line is : " + (scan.iSourceLineNr+1));
									// skip everything until hit the :
									// what if we dont have a : ?
									while( !scan.currentToken.tokenStr.equals(":") )
									{
										//scan.currentToken.printToken();
										scan.getNext();
									}
									

									//System.out.println("\t\t\t\t\t Line after value list is : " + (scan.iSourceLineNr));

									break;
								}
							}
			
							scan.getNext(); // iterate the cursor to next value in the 'when' value list
						}
						if( oldLine != scan.iSourceLineNr )
							error("Missing \':\' terminator for when on Line %s", whenLine );
						// should be at the : at this point
						
						if( !scan.currentToken.tokenStr.equals(":"))
						{
							error("Expected \':\' terminator for \'when\'");
						}
						
						if( compareCheck )
						{
							resTrueStmts = statements(true);
							// now run the rest of the select until endselect, default, or when
							
							//scan.currentToken.printToken();
							
							// then after a true "when" case
							// execute the remaining select when statements as false until endselect
							try
							{
								while(!resFalseStmts.terminatingStr.equals("endselect"))
								{
									resFalseStmts = statements(false);
								}
							}
							catch (Exception e)
							{
								error("Missing endselect terminator for select on Line %s", selectLineNum);
							}
						}
						else
						{
							resFalseStmts = statements(false);
						}

						break;
					case "default":
						defaultCheck = true;
						// always run the default if it reaches here
						//scan.currentToken.printToken();
						scan.getNext();
						
						if( !scan.currentToken.tokenStr.equals(":"))
						{
							errorWithCurrent("Expected \':\' terminator for \'default\'");
						}
						resTrueStmts = statements(true);
						break;
					case "endselect":
						error("Missing \'default\' for select on Line %s", selectLineNum);
						break;
					default:
						error("Missing \'endselect\' terminator keyword for select on Line %s", selectLineNum);
				}
			}
			//System.out.printf(">---> whenCheck=%s and defaultCheck=%s\n", whenCheck, defaultCheck);
			if( whenCheck == false )
			{
				if(defaultCheck == false )
					error("Expected \'default\' case for select on Line %s", selectLineNum);
			}
		}
		else // ignoring execution
		{
			//System.out.println("  >>>> in selectStmt(): FALSE EXECUTION");
			//scan.currentToken.printToken();
			
			// but we must also still check for the default case
			
			
			while(!resFalseStmts.terminatingStr.equals("endselect"))
			{
				resFalseStmts = statements(false);
			}

			if( resFalseStmts.terminatingStr.equals("endselect"))
			{

				if( scan.currentToken.subClassif != Token.END )
					errorWithCurrent("expected terminating \'endselect\' for select");
				
			}
		}
		
		if( !scan.currentToken.tokenStr.equals("endselect"))
		{
			error("Expected \'endselect\' terminator for \'select\'");
		}
		
		scan.getNext();
		//scan.currentToken.printToken();

		if( !scan.currentToken.tokenStr.equals(";"))
		{
			error("Expected \':\' terminator for \'endselect\'");
		}
		return resCond;
	}
	/**
	 * Assumptions: 
	 * <br> - 'for' is on the current token
	 * <br> - We have a Scanner.setPosition() function
	 * <br>Purpose:
	 * <br>For loop control flow that loops through a block of statements within itself
	 * <br> Similar to whileStmts().
	 * @param bExec
	 * @return ResultValue
	 * @throws Exception
	 */
	ResultValue forStmt(boolean bExec) throws Exception
	{
		ResultValue resCond = new ResultValue();
		ResultValue resStmts = new ResultValue();
		ResultValue resControl = null;
		ResultValue resSource = null;
		String controlName = "";
		int iSavedSourceLineNr = scan.currentToken.iSourceLineNr;
		int iSavedColPos = scan.currentToken.iColPos;
		int iLimit = 0;
		int iIncr = 1;

		// NOTE: if the current token is 'for', our expected nextToken is 
		// and OPERAND/IDENTIFIER variable for possible assignment
		// although we will not allow programmer to change the limit and incr but still allow
		// ControlVariable value to be manipulated. the CV should be checked against the
		// global SymbolTable and StorageManager, or implicitly declared if not in SymbolTable/StorMgr
		// Ex:   	for i = 3 to 10 by 1:
		//         	    statements
		//       	endfor;

		if( bExec ) // execute true side
		{
	    	scan.getNext(); // move past the 'for' keyword
	    	//scan.currentToken.printToken();
	    	// should be on an identifier right now
	    		// check symbol table for the value
	    		// if not exist, implicit assignmentStmt?
	    		// check nextToken if its an '='
	    			// if its a 'in'
	    				// go to 
	    	
	    	
			// Check symbol table if already declared
			//scan.currentToken.printToken();
	    	controlName = scan.currentToken.tokenStr;
			STEntry entry = st.getSymbol( scan.currentToken.tokenStr );
			//if( entry != null)
			//	errorWithCurrent("Previously declared variable");
	    	if( entry == null )
	    	{
	    		// add to symbol table since this should be a new declaration
	    		STIdentifier iden = new STIdentifier(
	    				scan.currentToken.tokenStr		    // the identifier name itself, symbol
	    				, Token.OPERAND // primClassif
	    				, Token.INTEGER 		// declare type
	    				, 0 	// structure
	    				, 0 			// param
	    				, 0 			// nonLocal
	    				);
	    		st.putSymbol(scan.currentToken.tokenStr, iden);
	    		
//	    		resControl = assignmentStmt( true );
//	    		
//	    		if( st.getSymbol(scan.currentToken.tokenStr).equals(null) )
//	    		{
//	    			System.out.println("in forStmt(): Variable " +  scan.currentToken.tokenStr  + " declared");
//	    		}
	    		
	    	}
	    	else // lets retrieve the value and use for Control Variable
	    	{
	    		
	    		
	    	}
	    	
	    	
	    	scan.getNext(); // advance the cursor past the identifier
	    	// after this we have several options
	    	/* Possible branches after our 'for CV' part of the for-loop statement
	    	 * 
	    	 *  1. Following the identifier is an '='
	    	 *  2. Following the identifier is keyword 'in'
	    	 * 
	    	 */
			
			if (scan.currentToken.tokenStr.equals("in"))
			{
	    		// for char in string or for item in array
	    		
	    		//resControl = assignmentStmt( true );
	    		// we should retrieve the result value from the variable after in

	    		resSource = preExpr();
	    		
	    		//scan.currentToken.printToken();

	    		// DEBUG print array elements
//	    		int i = 0;
//	    		for (ResultValue res : resSource.valueList)
//	    		{
//	    			System.out.printf(">>> in forStmt() on %s[%s]=%s\n", scan.currentToken.tokenStr, i, res.value);
//	    			i++;
//	    		}
	    		
	    		scan.getNext();
	    		if( !scan.currentToken.tokenStr.equals(":"))
	    		{
	    			errorWithCurrent("Expected \':\' terminator for control statement");
	    		}
	    		
	    		// start the for loop for 'in' iterator
	    		
	    		iLimit = resSource.arraySize;
	    		
	    		for( int index = 0; index < iLimit; index += iIncr)
	    		{
	    			if ( !resSource.valueList.get(index).value.equals("null"))
	    			{
	    				storageMgr.assign(controlName, resSource.valueList.get(index));
	    				//System.out.println("Printing");
	    				resCond = statements(true);
	    				
	    				// if ended on break, break out of loop
	    				//if(resCond.terminatingStr.equals("break")){
	    					//break;
	    				//}
	    				//System.out.println("Terminated: " + resCond.terminatingStr );
	    				scan.setPosition(iSavedSourceLineNr+1 , iSavedColPos);
	    			}
	    		}
	    		
	    		// now run statements of false condition until hit end for
				resCond = statements(false);
				if( resCond.terminatingStr.equals("endfor") == false )
					errorWithCurrent("expected endfor");
	    	

	    	} 
	    	else if(scan.currentToken.tokenStr.equals("="))
	    	{
	    		// counting for
	    		// check if cv has been previously declared

    			resControl = preExpr();
    			storageMgr.assign(controlName, resControl);
    			scan.getNext();
    			//scan.currentToken.printToken();

    			if(scan.currentToken.tokenStr.equals("to"))
    			{
    				// assign limit
    				//scan.getNext(); // get rid of the "to"
    				resSource = preExpr(); // does a scan.getNext() inside to grab an operand, ( or )
    				//scan.currentToken.printToken();
    				iLimit = Integer.parseInt(resSource.value);
    				
    				
    				if(scan.nextToken.tokenStr.equals("by"))
    				{
    					// change increment
    					scan.getNext(); // should be "by" 

    					//scan.getNext(); // should be incr value (currently guaranteed to be positive number in test input)
    					
    					ResultValue resIncr = preExpr();
	
    					
    					iIncr = Integer.parseInt(resIncr.value);
    					//iIncr = Integer.parseInt(storageMgr.getVariableValue(this, scan.currentToken.tokenStr).value);
    				} 
    				else 
    				{
    					iIncr = 1;
    				}
    				
    			}
    			else 
    			{
    				// syntax error
    				errorWithCurrent("expected to after cv = sv assignment, found " +
    				scan.currentToken.tokenStr);
    			}
    			
    			// perform for loop
        		for( int index = 0; index < iLimit; index += iIncr)
	    		{

    				resCond = statements(true);	// run the statements for this for-loop block
    				
    				// grab the ControlValue from storage manager
    				resControl = storageMgr.getVariableValue(this, controlName);
    				
    				// Increase by the increment
    				resControl.value = String.format("%s", Integer.parseInt(resControl.value) + iIncr);
    				
    				// stick back into StorageManager
    				storageMgr.assign(controlName, resControl);
    				
    				//if(resCond.terminatingStr.equals("break")) break;
    				
    				scan.setPosition(iSavedSourceLineNr+1 , 0); // reset the position to the top of the loop
    														    // under the 'for' statement line
    				
	    		}
	    		
	    		// now run statements of false condition until hit end for
				resCond = statements(false);
				if( resCond.terminatingStr.equals("endfor") == false )
					errorWithCurrent("expected endfor");
	    	}
	    	else if(scan.currentToken.tokenStr.equals("from"))
	    	{
	    		// lets get the next token which should be an operand identifier for a String var
	    		
	    		resSource = preExpr();
	    		
				//System.out.println(" in forStmt(): after FROM, value is " + resSource.value);

	    		if( resSource.resultType != Token.STRING )
	    		{
	    			errorWithCurrent("Expected STRING variable after \'from\'");
	    		}
	    		
	    		scan.getNext();
	    		
	    		
				//System.out.println(" >>>>> In forStmt(), source=" + resSource.arraySize);

    			if( scan.currentToken.tokenStr.equals("by")) // get the delimiter
    			{
    				resControl = preExpr();
    				
    				System.out.println(" in forStmt(): after By, value is " + resControl.value);

    	    		scan.getNext();

    			}
    			else
    			{
    				errorWithCurrent("Expected \'by\' after from");
    			}
    			
    			//scan.getNext(); // eat the :
    			
    			//scan.currentToken.printToken();
    			
    			// perform the for loop, iterating by delimiter
    			
    			iLimit = resSource.arraySize;
				StringBuilder s = new StringBuilder();

    			
    			// on each character of this string
    			for( int i = 0; i<iLimit; i++)
    			{
    				//scan.currentToken.printToken();
    				if( resSource.valueList.get(i).value.equals(resControl.value))
    				{
    					ResultValue result = new ResultValue();
    					result.value = s.toString();
    					//result.arraySize = iLimit+s.toString().length();
    					result.resultType = Token.STRING; // implicit STRING type on cv
    					result.structure = 0;
        				storageMgr.assign(controlName, result);
        				//System.out.println("Printing");
        				resCond = statements(true);
        				scan.setPosition(iSavedSourceLineNr+1, 0);
        				s = new StringBuilder();
    				}
    				else
    					s.append( resSource.valueList.get(i).value);
    			}	
    			
    			// print remainder of the string
				//System.out.println("PRINT is " + s.toString());
				ResultValue result = new ResultValue();
				result.value = s.toString();
				result.resultType = Token.STRING;
				result.structure = 0;
				storageMgr.assign(controlName, result);
				//System.out.println("Printing");
				resCond = statements(true);
    			
	    	} 
	    	else 
	    	{
	    		// syntax error
	    		errorWithCurrent("expected either cv = sv or x in y format");
	    	}
			
		} // -------- end TRUE SIDE
		else // false side, go through but do not execute the block of statements until endfor reached
		{
			//System.out.printf("\t\tDEBUG: Inside whileStmt(), ignoring execution at line %s\n", scan.currentToken.iSourceLineNr+1 );
			resStmts = statements(false);
			if( resStmts.terminatingStr.equals("endfor") == false )
				errorWithCurrent("expected endfor");
		} // -------- end FALSE SIDE
		
		
		return resCond;
	}
	// assumptions: 'while' is in the current token
	ResultValue whileStmt(boolean bExec) throws Exception
	{
		ResultValue resCond = new ResultValue();
		ResultValue resStmts = new ResultValue();
		//Token savedToken = scan.currentToken;
		//this.savedToken = scan.currentToken;
		int iSavedSourceLineNr = scan.currentToken.iSourceLineNr;
		int iSavedColPos = scan.currentToken.iColPos;

		if( bExec ) // true side
		{
			resCond = preExpr();
			if( resCond.value.equals("T"))
			{
				while( resCond.value.equals("T") ) // loop until while condition no longer valid
				{

					// execute statements
					resStmts = statements( true );
					// hit the endwhile
					
					/*
					if( resStmts.terminatingStr.equals("break")){
						statements(false); // get to endwhile
						break;
					}
					if(resStmts.terminatingStr.equals("continue")){
						statements(false); // get to endwhile
						//continue;
					}*/
					if( resStmts.terminatingStr.equals("endwhile") == false )
						errorWithCurrent("expected endwhile");

					//System.out.printf("\t\tDEBUG: Inside whileStmt() loop: Cond eval to %s\n", resCond.value );
					// set position back to saved 
					scan.setPosition( iSavedSourceLineNr, iSavedColPos );

					// evaluate while condition
					resCond = preExpr();
					if(  resCond.value.equals("F") )
					{
						//System.out.printf("\t\tDEBUG: Inside whileStmt(): finished loop\n" );

						// execute statements
						resStmts = statements( false );
						// hit the endwhile
						if( resStmts.terminatingStr.equals("endwhile") == false )
							errorWithCurrent("expected endwhile");
						//return resCond;
						//break;
					}

				}

			}
			else // the condition isn't valid, ignore until next endwhile;
			{

				resStmts = statements(false);
				if( resStmts.terminatingStr.equals("endwhile") == false )
					errorWithCurrent("expected endwhile");

				
			}
		}
		else // false side
		{
			//System.out.printf("\t\tDEBUG: Inside whileStmt(), ignoring execution at line %s\n", scan.currentToken.iSourceLineNr+1 );
			resStmts = statements(false);
			if( resStmts.terminatingStr.equals("endwhile") == false )
				errorWithCurrent("expected endwhile");
		}

		return resCond;
	}
	
	
	// assumptions: 'if' or 'else' is on the current token
	ResultValue ifStmt( boolean bExec ) throws Exception
	{
		ResultValue resCond = new ResultValue();
		ResultValue resTrueStmts = new ResultValue();
		ResultValue resFalseStmts = new ResultValue();
		//System.out.printf("\t\tDEBUG: Inside ifStmt(), current token is \'%s\'\n", scan.currentToken.tokenStr );

		if( bExec ) // true side
		{
			//System.out.printf("\t\tDEBUG: Inside ifStmt(), executing at line %s\n", scan.currentToken.iSourceLineNr+1 );

			resCond = preExpr(); // evaluate the condition inside 
			scan.getNext(); // eat : terminator
			//System.out.printf("\t\tDEBUG: Inside ifStmt(): Cond eval to %s\n", resCond.value );
			
			if( resCond.value.equals("T") ) // if cond is TRUE
			{
				//System.out.printf("\t\tDEBUG: Inside ifStmt(), doing T side\n" );
				resTrueStmts = statements( true ); // run statements for if block until else or endif hit
				if( resTrueStmts.terminatingStr.equals("else")) // if-endif block
				{
					scan.getNext();
					//if( !scan.getNext().equals(":"))
					//		errorWithCurrent("expected \':\'");
					resFalseStmts = statements( false ); // run statements for else block until endif hit
					if( resFalseStmts.terminatingStr.equals("endif") == false) // if-endif block
						errorWithCurrent("expected endif after else");
				}
				//if( resTrueStmts.terminatingStr.equals("endif") == false )
				if( scan.currentToken.tokenStr.equals("endif") == false )
				{
					//scan.currentToken.printToken();
					errorWithCurrent("expected endif");
					//scan.getNext();
				}


			}
			else if(resCond.value.equals("F"))
			{
				//System.out.printf("\t\tDEBUG: Inside ifStmt(), doing F side\n" );
				// do F cond with else block statements until endif;

				//System.out.printf("\t\tDEBUG: Inside ifStmt(), current token is \'%s\'\n", scan.currentToken.tokenStr );

				resFalseStmts = statements( false ); // run statements for if block until else or endif hit
				if( resFalseStmts.terminatingStr.equals("else")) // if-endif block
				{
					scan.getNext();
					resTrueStmts = statements( true ); // run statements for else block until endif hit
					if( resTrueStmts.terminatingStr.equals("endif") == false) // if-endif block
						errorWithCurrent("expected endif after else");
				}
				if( resFalseStmts.terminatingStr.equals("endif"))
				{
					//scan.getNext();
				}
			} 
			
		}
		else // ignoring execution
		{
			resTrueStmts = statements(false);
			if( resTrueStmts.terminatingStr.equals("else"))
			{
				// error check for terminating :
				scan.getNext();
				resFalseStmts = statements(false);
				if( resFalseStmts.terminatingStr.equals("endif") == false )
				{
					if( scan.currentToken.subClassif != Token.END )
						errorWithCurrent("expected endif after else");
				}
			}

			//resFalseStmts = statements(false);
			if( resFalseStmts.terminatingStr.equals("endif") == false)
			{
				//scan.currentToken.printToken();
				if( scan.currentToken.subClassif != Token.END )
					errorWithCurrent("expected endif after else");
			}
			scan.getNext();
		}
		return resCond;
	}

	/**
	 * Reserves a spot in the storage manager for the declaration statement.
	 * <br> Assumes the current token is the DECLARE sub-type of CONTROL ( i.e. Int, String, ... )
	 * @param bExec 
	 * @return
	 */
	ResultValue declareStmt( boolean bExec ) throws Exception
	{
		//System.out.println(">>> in declareStmt()");
		//scan.nextToken.printToken();
		ResultValue result = new ResultValue();
		STIdentifier iden = null;
		// error check for DECLARE type token
		if( scan.currentToken.subClassif != Token.DECLARE )
			error("Invalid declaration statement");
		
		String type = scan.currentToken.tokenStr;
		
		// grab the next token which should be an identifier
		scan.getNext();
		
		
		if( scan.currentToken.subClassif != Token.IDENTIFIER )
			errorWithCurrent("Invalid identifier for declaration or attempting to use reserved keyword");
		// copy all of the identifier token for backtracking
		Token backtrackToken = new Token();
		backtrackToken.tokenStr = scan.currentToken.tokenStr;
		backtrackToken.primClassif = scan.currentToken.primClassif;
		backtrackToken.subClassif = scan.currentToken.subClassif;	
		backtrackToken.iSourceLineNr = scan.currentToken.iSourceLineNr;
		backtrackToken.iColPos = scan.currentToken.iColPos;
		String name = scan.currentToken.tokenStr;
	
		// Make identifier a ResultValue object
		result = scan.currentToken.toResult();
		
		int dclType = 0;
		int structure = 0; // data structure (primitive(0), fixed array(1), unbound array(2))
		int arraySize = 0; // default array size unless specified by list or bracket elements
		ArrayList<ResultValue> resValues = null;
		switch( type )
		{
			case "Int":
				//System.out.println("DECLARING INT");
				dclType = Token.INTEGER;
				break;
			case "Float":
				dclType = Token.FLOAT;
				break;
			case "String":
				dclType = Token.STRING; //this will be an array of characters
				structure = 2; // unbound array?
				break;
			case "Bool":
				dclType = Token.BOOLEAN;
				break;
			case "Date":
				dclType = Token.DATE;
				break;
			default:
				error("Declaration type is not Int, Float, String, Bool, or Date");
		}
		
		// Check symbol table if already declared
		//scan.currentToken.printToken();
		//STEntry entry = st.getSymbol( scan.currentToken.tokenStr );
		//if( entry != null)
		//	errorWithCurrent("Previously declared variable");
		
		// lookahead check array structure declaration
		if( scan.nextToken.tokenStr.equals("[") )
		{
			// we have a [ next, eat it
			//System.out.println(">>> FOUND [");
			resValues = new ArrayList<ResultValue>(); // now lets allocate for impending list of values
													  // for this array declaration
			scan.getNext();

			// is it only []'s?
			// is the MAXELEM specified?
			if( scan.nextToken.primClassif == Token.OPERAND )
			{

				//scan.getNext();
				
				//System.out.println(">>> FOUND " + scan.currentToken.tokenStr);

				result = preExpr();
				//arraySize = Integer.parseInt(scan.currentToken.tokenStr);
				declaredArraySize = Integer.parseInt( result.value );
				result.arraySize = declaredArraySize;
				arraySize = declaredArraySize;
				//resValues = new ArrayList<ResultValue>(arraySize); // now lets allocate for impending list of values
				  // for this array declaration
				//System.out.println(">>> FOUND " + result.value);
				
			}
			else if(scan.nextToken.tokenStr.equals("(") )
			{
				result = preExpr();
				declaredArraySize = Integer.parseInt( result.value );
				result.arraySize = declaredArraySize;
				arraySize = declaredArraySize;

			}
			if( scan.nextToken.tokenStr.equals("]"))
			{
				//System.out.println(">>> FOUND ]");
				scan.getNext();
				
				// the value list MUST be provided in assignment now
				// unless size was grabbed within brackets

			}
			else
			{
				errorWithCurrent("Expected size or \']\' terminator after array initialization");
			}
			
			structure = 1; // fixed array
		}

		// add to symbol table since this should be a new declaration
		iden = new STIdentifier(
				name 		    // the identifier name itself, symbol
				, Token.OPERAND // primClassif
				, dclType  		// declare type
				, structure 	// structure
				, 0 			// param
				, 0 			// nonLocal
				);
		st.putSymbol(name, iden);
		
		// copy back backtrackToken no matter what for the identifier name
		// copy nextToken into currentToken
		scan.currentToken.tokenStr = backtrackToken.tokenStr;
		scan.currentToken.primClassif = backtrackToken.primClassif;
		scan.currentToken.subClassif = backtrackToken.subClassif;	
		scan.currentToken.iSourceLineNr = backtrackToken.iSourceLineNr;
		scan.currentToken.iColPos = backtrackToken.iColPos;
		//scan.currentToken.printToken();
		//scan.nextToken.printToken();
		// check if we have a terminator
		if( scan.nextToken.tokenStr.equals(";") )
		{
			// somehow stick it into the StorageManager without a value
			//scan.currentToken.printToken();
			result.value = "";
			
			if( arraySize > 0 )
			{
				while( arraySize > 0 )
				{
					ResultValue blank = new ResultValue();
					blank.resultType = Token.INTEGER;
					blank.value = "null";
					result.valueList.add(blank);
					arraySize--;

				}
			}
			
			result = storageMgr.assign(name, result);
			scan.getNext(); // eat ; terminator
		}
		else if ( scan.nextToken.tokenStr.equals("=")) // lookahead for assignment operator
		{
			//System.out.println(">>> in declareStmt(), assigning value");
			
			// at this point we need to check if we have an array declaration
			if( structure == 1 ) // if an array
			{
				 // reserve memory in storage manager or the flag
//				System.out.println(">>> RESERVING MEMORY FOR ARRAY size=" + result.value + " structType=" + iden.structure);
//				//scan.currentToken.printToken();
//				STIdentifier entry1 = (STIdentifier) st.getSymbol( scan.currentToken.tokenStr );
//				if( entry1 == null)
//					errorWithCurrent("Attempting to use undeclared variable for assignment");
//	    		System.out.printf(">>> in declareStmt(): STentry \n%s\n%s\n%s\n%s\n"
//	    				, entry1.symbol
//	    				, entry1.primClassif
//	    				, entry1.dclType
//	    				, entry1.structure);


			}
			
			result = assignmentStmt( true );
			
			// DEBUG print array elements
//			int i = 0;
//			for (ResultValue res : result.valueList)
//			{
//				System.out.printf(">>> in declareStmt() on %s[%s]=%s\n", name, i, res.value);
//				i++;
//
//			}

			result.structure = structure;
			result = storageMgr.assign(name, result);
			
			//reset the private static array size
			declaredArraySize = 0;
			//System.out.println(">>> in declareStmt(), after assigning :"+ result.value);
			//scan.getNext(); // eat ; terminator
			//scan.currentToken.printToken();
		}
		else // invalid declaration statement
		{
			error("Expected terminating symbol \';\'");
		}
		

		//scan.currentToken.printToken();
		
		//scan.getNext(); // eat ; terminator
		//System.out.printf("\t\tDEBUG: Inside declareStmt(): value of var \'%s\' = %s\n", scan.currentToken.tokenStr, result.value);

		
		return result;
	}
	
	/**
	 * Invokes the function for "debug" which prints to STDOUT debugging aids
	 * <p> HavaBol Statement: debug {type} {onOff}
	 * <p> Debugging types:
	 * <ul>
	 * 	 <li> Token - print the token infomation for the current token returned by scan.getNext() </li>
	 *	 <li> Expr - print the result of each expression evaluation </li>
	 * 	 <li> Assign - print the variable and value for an assignment</li>
	 * </ul>
	 * <p> Assumptions: 
	 * <br> Assumes the current token is the function "debug" and nextToken is the type of debugging
	 * @return 
	 *
	 */
	ResultValue debugStmt( boolean bExec ) throws Exception
	{
		// TODO
		
		return null;
	}
	
	/**
	 * Invokes the function for "print" to output on STDOUT <br>
	 * Assumptions: <br>
	 * Assumes in the first call the current token is the function "print" and nextToken is '('
	 * then in subsequent recursive calls its a '(' or an Operand.
	 *
	 */
	ResultValue printStmt( boolean bExec ) throws Exception
	{
		ResultValue result = null;

		if( bExec )
		{
			scan.getNext(); // grab the string, variable, "(" or ")"
		   // scan.currentToken.printToken();

		    switch (scan.currentToken.primClassif)
		    {
		    	case Token.SEPARATOR:
		    		switch( scan.currentToken.tokenStr )
		    		{
		    			case "(":
		    				if( parenCount < 1 )
		    				{
		    					parenCount++;
		    					result = printStmt( true );
		    				}
		    				else
		    				{
		    					parenCount++;	    					
		    					//System.out.println( "PAREN COUNT: " + parenCount );
		    					//scan.currentToken.printToken();

		    					result = invokePreFunc();
		    					System.out.print( result.value );;
		    					if( scan.nextToken.primClassif == Token.OPERAND )
		    						return printStmt( true );
		    					if( scan.currentToken.tokenStr.equals(")") )
		    					{
		    						//scan.currentToken.printToken();
		    						return printStmt( true );
		    					}
		    					System.out.println();
		    					//printStmt(true);
		    					//System.out.println( "   >>>> PRINTING FUNC");
		    				}
			    			
			    			break;
		    			case ")":
		    				parenCount--;
	    					//System.out.println( " NEXT = >> " + scan.nextToken.tokenStr + " <<");

		    				if( scan.nextToken.tokenStr.equals(")") == true )
		    				{
		    					System.out.println( " FOUND END RPAREN");
		    				}
		    				result = new ResultValue();
		    				result.resultType = Token.RT_PAREN;
		    				System.out.println();
		    				return result;
	    				default:
	    					errorWithCurrent("Expected terminating ')'");
		    		}
		    		break;
		    	case Token.OPERAND:
		    		switch( scan.currentToken.subClassif )
		    		{
			    		case Token.STRING:
			    			//result = preExpr();
			    			// maybe process the special chars within the string?
			    			String tokenStrTemp = Utility.formatStringValue(this, scan.currentToken.tokenStr);
						
				    		//System.out.print(scan.currentToken.tokenStr);
							System.out.print(tokenStrTemp);
							
				    		
				    		return printStmt( true );
			    		case Token.IDENTIFIER: // could be Ints, String Literal values. etc...
			    			//scan.currentToken.printToken();
			    			
			    			String str = String.format( storageMgr.getVariableValue( this
			    							, scan.currentToken.tokenStr ).value);
			    			
			    			
			    			str = Utility.formatStringValue(this, str);
			    			System.out.printf("%s ", str );
			    			//return storageMgr.getVariableValue( this, scan.currentToken.tokenStr );
			    			return printStmt( true );
		            	case Token.INTEGER: 
		            	case Token.FLOAT:
		            	case Token.DATE:
		            	case Token.BOOLEAN:
			    			
			    			String strConst = scan.currentToken.tokenStr;
			    			
			    			strConst = Utility.formatStringValue(this, strConst);
			    			System.out.printf("%s ", strConst );

			    			return printStmt( true );
			    		default:
			    			errorWithCurrent("Expected string literal, variable or constant for \'print\'");
		    		}
		    	default:
		    		errorWithCurrent("Expected \')\'");
		    }
		}
		else // ignoring executing
		{
			//System.out.printf("\t\tDEBUG: Inside printStmt(), ignoring exec line %s\n", scan.currentToken.iSourceLineNr+1 );
		}
		scan.getNext(); // eat the ; terminator
		//scan.currentToken.printToken();
		/*
	    scan.getNext();
	    if (scan.currentToken.tokenStr.equals(";") == false )
	    {
			errorWithCurrent("Expected ; end");

	    }
	    */
	    return result;
	}
	
	/**
	 * Assigns the variable a value based on the operator.
	 * <p> Assumptions:
	 * <ol> 
	 *  <li> The current token is on the variable name. </li>
	 *  <li> Have a preExpr() subroutine which parses an prefix expr and returns the result for that preExpr. </li>
	 *  <li> Not yet handling subscripts or slices. </li>
	 *  <li> Have a Numeric constructor that converts a ResultValue object into a Numeric object. </li>
	 * </ol>  
	 * <p> Notes:
	 * <ol>
	 *  <li> Right side of assignment operator is an expression. We call it resOperand2 </li>
	 *  <li> Left side of assignment operator is the target variable.
	 *       For "+=" and "-=", it is also the resOperand1 of "+" or "-". </li>
	 * </ol>
	 * @param bExec Flag for conditional statements indicating the branch for execution.
	 * @return ResultValue which is what was assigned to the target.
	 * @throws Exception
	 */
	ResultValue assignmentStmt( Boolean bExec ) throws Exception
	{
		//scan.currentToken.printToken();
	    ResultValue res = null;
	    int index = -100;
	    if( bExec )
	    {
		    //scan.currentToken.printToken();
		    if (scan.currentToken.subClassif != Token.IDENTIFIER)
		    		errorWithCurrent("Expected a variable for the target of an assignment");
		    
		    //check if it has been declared
			STIdentifier entry = (STIdentifier) st.getSymbol( scan.currentToken.tokenStr );
			if( entry == null)
				errorWithCurrent("Attempting to use undeclared variable for assignment");
		    
		    String variableStr = scan.currentToken.tokenStr;
	    	//System.out.println("FOUND IDENTIFIER: " + scan.currentToken.tokenStr + " Line: " + (scan.iSourceLineNr+1));

		    
			if ( scan.nextToken.tokenStr.equals("[" ) ) // and have a bracket, we better expect
			{  											// to have an operand or expression next
														// to assign into this slot		
				scan.getNext();
			//	scan.currentToken.printToken();
				index = 0;
				res = preExpr();
				//System.out.println("OPERAND type = " + res.resultType);

				res = Utility.toInteger(this, res);
				index = Integer.parseInt(res.value);

				// get the expr or operand that will evaluate to an integer
				// that will be the subscript to reference in the array

	        	
				// we should have the ] on the current token by now
				// get on it
	        	scan.getNext();
	        	//scan.currentToken.printToken();
			}

		    // get the assignment operator and check it
        	//scan.currentToken.printToken();

		    scan.getNext();
		    if (scan.currentToken.primClassif != Token.OPERATOR) 
		    		error("Expected assignment operator after variable \'%s\'", scan.currentToken.tokenStr);

		    String operatorStr = scan.currentToken.tokenStr;
		    ResultValue resO2 = null;
		    ResultValue resO1;
		    Numeric nOp2;  // numeric value of second operand
		    Numeric nOp1;  // numeric value of first operand
		
		    
		    switch(operatorStr)
		    {
		    	case "(":
		    		resO2 = preExpr();
		        	res = storageMgr.assign(variableStr, resO2);
		    		//System.out.printf("\t\tDEBUG: Assigning %s into %s\n", resO2.value, variableStr);

		        	break;
		    	case "=":
		    		//scan.currentToken.printToken();
		    		
		    		// we could have an array declaration with a subsequent list of values 
		    		// that could be expressions separated by commas up until the ;
		    		// Note: this could be annoying
		    		// Note: Also keep in mind of data coercions for array declarations
		    		// Ex: Int weirdM[] = 10, "20", 30.5;  this is Int, String, Float all as Int elements
		    		//System.out.printf(">>> in assignmentStmt(): array assigning to %s\n", entry.structure);

		    		
		    		if( entry.structure > 0 ) // if have an array, bound or unbound
		    		{
		    			// handle array on declaration stuff if the operator is =
		    			if( index > -2 )
		    			{
				    		//scan.currentToken.printToken();

		    				resO2 = preArrayAssignmentElementRef(index, variableStr);
				        	res = storageMgr.assign(variableStr, resO2 );
		    			}
		    			else
		    			{
				    		if ( entry.dclType == Token.STRING ) // String array
				    		{

				    			resO2 = preArrayStringAssignment( variableStr );
				    			res = storageMgr.assign(variableStr, resO2 );

				    		}
				    		else
				    		{
				    			//scan.nextToken.printToken();
				    			if( scan.nextToken.subClassif != Token.IDENTIFIER )
				    				res = preArrayAssignmentDeclare( variableStr );
				    			else // have array to array assignment
				    			{
				    				resO2 = preArrayToArrayAssignment( variableStr );
				    				res = storageMgr.assign(variableStr, resO2);
				    			}
				    			//storageMgr.assign(variableStr, res);
			    				// no storage manager assignment here since the declareStmt()
			    				// before it will do the assigning, just for arrays though
			    				// sorry about that 
				    			
				    		}

		    			}
		    		}
		    		else
		    		{

			    		resO2 = preExpr();
			    		
		    			// do we have a date? lets check
		    			if( entry.dclType == Token.DATE )
		    			{
		    				//scan.currentToken.printToken();
		    				ResultValue resEval = Utility.validateDate(this, resO2);
		    				
		    			}
			        	res = storageMgr.assign(variableStr, resO2 );
			        	scan.getNext(); // advance to possibly the terminator
		    		}    		
		    		
		    		//System.out.printf("\t\tDEBUG: Assigning %s into %s\n", resO2.value, variableStr);
		        	break;
		    	case "-":
		    		if(scan.nextToken.tokenStr.equals("=")){
		    		scan.getNext();
	        		resO2 = preExpr();   
			        // expression must be numeric, raise exception if not
			        //TODO ??
			        // Since it is numeric, we need value of target variable 
			        resO1 = storageMgr.getVariableValue( this, variableStr);
			        // target variable must be numeric
			        // TODO ??
			        
			        // subtract 2nd operand from first and assign it
			        //TODO res = ??
			        res = storageMgr.assign(variableStr, Utility.minus(this, resO1, resO2));
		        	break;
		    		} else error("Expected = after -");
		    	case "+":
		    		if(scan.nextToken.tokenStr.equals("=")){
		    		scan.getNext();
		    		resO2 = preExpr();
		    		resO1 = storageMgr.getVariableValue(this, variableStr);
		    		res = storageMgr.assign(variableStr, Utility.add(this, resO1, resO2));
		    		// fill it in yourself
		    		break;
		    		} else error("Expected = after +");
		    	default:
		    		error("Expected assignment operator");
	    	}   
		    
	    }
	    //if( scan.getNext().trim().equals(";") == false )
	    //	errorWithCurrent("Expected \';\' terminator");
//	    if( scan.currentToken.tokenStr.contains(";") == false  )
//	    {
//	    	errorWithCurrent("Expected \';\' terminating assignment statement");
//	    	//System.out.println("FOUND SEMICOLON AT END");
//	    }
	   // scan.getNext(); // eat the ; terminator

	    return res;
	}


	// Assumptions: the current token is an OPERAND or paren Separators
	ResultValue preExpr() throws Exception
	{
	    scan.getNext();  // get the operand, '(' or ')'
	    //scan.currentToken.printToken(); // use for debugging
	    //System.out.printf("\t\tDEBUG: Inside preExpr(), line %s\n", scan.currentToken.iSourceLineNr+1 );

	    switch (scan.currentToken.primClassif){
	    	case Token.SEPARATOR:
	            switch (scan.currentToken.tokenStr)
	            {
	            	case "(":   
	 	                return invokePreFunc();

	            	case ")":
	 	                //scan.currentToken.printToken();
	                    // Why is this possible? not an error
	            		// When we have a variable # of args, marks end of the args list
	                    ResultValue res = new ResultValue();
	                    res.resultType = Token.RT_PAREN;
	                    return res;
	            	//case "[":
	            	//case "]":
            		default:
            			errorWithCurrent("Expecting \'(\' or \')\' separator for expression");
	            }
	            // gotta do returns
	    	case Token.OPERAND:
	            switch (scan.currentToken.subClassif)
	            {
	            	case Token.IDENTIFIER: // get variable data from Storage Manager

	                    return storageMgr.getVariableValue( this, scan.currentToken.tokenStr );
	                // fall through with these cases to convert CONSTANT token into ResultValue
	            	case Token.INTEGER: 
	            	case Token.FLOAT:
	            	case Token.DATE:
	            	case Token.STRING:
	            	case Token.BOOLEAN:
	            		//System.out.printf("\t\tDEBUG: Inside preExpr(): getting value for %s\n", scan.currentToken.tokenStr );

	                    // convert the value to a ResultValue object
	            		//scan.currentToken.printToken();
	                    return scan.currentToken.toResult();
	                default:
	                    errorWithCurrent("Expected operand for expression"); 
	            }
	        default:
	            errorWithCurrent("Invalid expression, expected operand, '(' or ')'"); 
	    }
		return null;
	}

	// Interp prefix function or operator call
	// Assumption:  '(' is in current token
	ResultValue invokePreFunc() throws Exception
	{
	    ResultValue res = null;
	    scan.getNext();  // get the operator or function name
	    
		//System.out.println(" >>>>> CURRRENT TOKEN " + scan.currentToken.tokenStr);
		//scan.currentToken.printToken();

	    switch (scan.currentToken.primClassif)
	    {

	    	case Token.OPERATOR:
	            switch (scan.currentToken.tokenStr)
	            {
	            	case "+":
	            		//scan.currentToken.printToken();
	                    res = preAdd();  // may want to support a variable num of args
	                    break;
	            	case "*": // fall through * and / operations
	            	case "/":
	                    res = preBinaryNumOp();
	                    break;
	                    
	            	case "-":
	                    res = preMinus();  
	                    break;
	                case "^":
	                	res = preExponent();
	                	break;
	            	case "==":
	            		res = preEqualTo();
	            		break;
	            	case ">=":
	            		res = preGreatEqualTo();
	            		break;
	            	case "<":
	            		res = preLessThan();
	            		break;
	            	case ">":
	            		res = preGreaterThan();
	            		break;
	            	case "<=":
	            		res = preLesserOrEquals();
	            		break;
	            	case "!=":
	            		res = preNotEqualTo();
	            		break;
	            	case "#":
	            		//scan.currentToken.printToken();
	            		res = preStringConcat();
	            		break;
	            	case "and":
	            		res = preAnd();
	            		break;
	            	case "or":
	            		res = preOr();
	            		break;
	            	case "not":
	            		res = preNot();
	            		break;
	            	case "IN":
						res = preInFunc();
						break;
					case "NOTIN":
						res = preNotInFunc();
						break;
	                default:
	                    errorWithCurrent ("Unknown operator for this function");
	            }
	            break;
	    	case Token.FUNCTION:
	    		switch ( scan.currentToken.subClassif )
	    		{
	    			case Token.BUILTIN:

	    				switch ( scan.currentToken.tokenStr )
	    				{
	    					case "print":
	    						res = printStmt(true);
	    						break;
	    					case "array": // given an array name and a subscript val/expr, return the element
	    						res = preArrayRef();
	    						break;
	    					case "LENGTH": // returns the numb of chars in a str
	    						res = preLengthFunc();
	    						break;
	    					case "ELEM": // returns subscript of highest populated
	    						         // element+1
	    						res = preElemFunc();
	    					    break;
	    					case "MAXELEM": // returns the declared number of elements
	    						res = preMaxElemFunc();
	    						break;
	    					case "SPACES": // returns T if the string is empty
	    						           // or only contains spaces
	    						res = preSpacesFunc();
	    						break;
	    					case "dateDiff":
	    						
	    						res = preDateDiffFunc();
	    						break;
	    					case "dateAdj":
	    						res = preDateAdjFunc();
	    						break;
	    					case "dateAge":
	    						res = preDateAgeFunc();
	    						break;
	    					default:
	    						errorWithCurrent("Unknown built-in function");
	    				}
	    				break;
	    			default:
	    				errorWithCurrent("Unknown function");
	    		}
	    		break;
	        default:
	            error ("Unknown function or operator, found %s'"
	                , scan.currentToken.tokenStr);
	    }
	    // What token should be the currentToken?
	    
	    // scan.currentToken.printToken();
	    if( scan.currentToken.tokenStr.equals(")"))
	    {

	    	//scan.getNext();
			//scan.currentToken.printToken();

	    	return res;
	    }
	    errorWithCurrent("Expected \')\' after arguments...");
	    return null; // this will not be reached
	}
	
	/**
	 * Assumptions: "dateDiff" is on the current token
	 * @return
	 */
	private ResultValue preDateDiffFunc() throws Exception
	{
		ResultValue resOut = new ResultValue();
		ResultValue resOp1 = null;
		ResultValue resOp2 = null;
		
		resOp1 = preExpr();
		resOp2 = preExpr();
		
		resOut = Date.dateDiff(this, resOp1, resOp2);
		
		//scan.currentToken.printToken();
		
		scan.getNext();
		
		return resOut;
	}
	
	private ResultValue preDateAdjFunc() throws Exception
	{
		ResultValue resOut = new ResultValue();
		ResultValue resOp1 = null;
		ResultValue resOp2 = null;
		
		resOp1 = preExpr();
		resOp2 = preExpr();
		
		resOut = Date.dateAdj( resOp1, resOp2);
		
		scan.getNext();
		
		return resOut;
	}
	
	private ResultValue preDateAgeFunc() throws Exception
	{
		ResultValue resOut = new ResultValue();
		ResultValue resOp1 = null;
		ResultValue resOp2 = null;
		
		resOp1 = preExpr();
		resOp2 = preExpr();
		
		 resOut = Date.dateAge(  resOp1, resOp2 );
	
		scan.getNext();
		
		return resOut;
	}

	/**
	 * Assumptions: the current token is '=' and the next token is an operand value.
	 * <br>
	 * Returns a ResultValue that holds a value list of ResultValues where each of those
	 * is a character in the token string value for assignment.
	 * @return
	 */
	private ResultValue preArrayStringAssignment(String name) throws Exception
	{
		ResultValue resOut = new ResultValue();
		ResultValue resWorking = null;
		
		// eat the '=', advance to operand
		//scan.getNext();
		STIdentifier entry1 = (STIdentifier) st.getSymbol( name );
		if( entry1 == null)
			errorWithCurrent("Attempting to use undeclared variable for assignment");
		
		//scan.currentToken.printToken();
		//resWorking = scan.currentToken.toResult();
		resWorking = preExpr();
		char[] charArr = scan.currentToken.tokenStr.toCharArray();
		resWorking.arraySize = charArr.length;
		resWorking.valueList = new ArrayList<ResultValue>(charArr.length);
//		System.out.printf(" in preArrayStringAssign(): returning %s length=%s \n"
//		, scan.currentToken.tokenStr
//		, charArr.length
//		//, resOut.value
//		);
		
		// for each character, make into a ResultValue to stick into the
		// String ResultValue list of elements
		for(int i = 0; i < charArr.length; i++)
		{
			ResultValue resChar = new ResultValue();
			//System.out.printf("%s[%s]=%c\n", name, i, charArr[i]);
			resChar.value = String.format("%c", charArr[i]); 
			resChar.resultType = Token.STRING;
			resWorking.valueList.add( resChar );
		}
		
		scan.getNext(); // advance past the operand
		//scan.currentToken.printToken();
		resOut = resWorking;
		return resOut;
	}
	
	/**
	 * Assumptions: the current token is "array"
	 * Returns the element at the subscript for a given array name and subscript
	 * @return
	 * @throws Exception
	 */
	private ResultValue preArrayRef() throws Exception
	{
		ResultValue resOut = new ResultValue();
		ResultValue resVar = null;
		ResultValue resSubscript = null;
		int index = 0;
		
		scan.getNext(); // move past the 'array' keyword
		if( scan.currentToken.subClassif != Token.IDENTIFIER ){
			errorWithCurrent("array reference function: 1st parameter is not a variable");
		}
		
		// we should have the operand that is the variable, check from symbol table
		STIdentifier entry1 = (STIdentifier) st.getSymbol( scan.currentToken.tokenStr );
		if( entry1 == null)
			errorWithCurrent("Attempting to use undeclared variable for assignment");
		
		// grab the result from the variable in StorageManagerr
		resVar = storageMgr.getVariableValue(this, entry1.symbol);
	
		
		//scan.getNext(); // go to the next parameter which should be an expr or constant as subscript
		//scan.currentToken.printToken();
		resSubscript = preExpr();
		//System.out.println("Index val " + resSubscript.value);
		try 
		{
			index = Integer.parseInt( resSubscript.value );
		}
		catch (Exception e)
		{
			errorWithCurrent("Not a valid subscript, must be Integer type");
		}
		// bounds check here
		if( index > resVar.arraySize || index < -1)
			error("Array indexing out of bounds: Size %s. Found \'%s\'", resVar.arraySize, scan.currentToken.tokenStr);
		
		resOut = resVar.valueList.get(index);
		
//		System.out.printf(" in preArrayRef(): returning %s[%s]=%s\n"
//				, entry1.symbol
//				, index
//				, resOut.value
//				);
		
		
	   // scan.currentToken.printToken();
		scan.getNext(); // advance past the expr
		
		return resOut;
	}
	
	/**
	 * Assumptions: the current token is '=' to prepare for preExpr()
	 * References the specified index to assign into the slot of the array;
	 * @return
	 * @throws Exception
	 */
	private ResultValue preArrayAssignmentElementRef(int index, String symbol) throws Exception
	{
		ResultValue resOut = new ResultValue();
		ResultValue resWorking = null;
		ResultValue resElement = preExpr();
		//scan.currentToken.printToken();
		
		// check symbol table 

		STIdentifier entry1 = (STIdentifier) st.getSymbol( symbol );
		if( entry1 == null)
			errorWithCurrent("Attempting to use undeclared variable for array reference");
		
		// ok lets retrieve the array list from Storage Manager
		resOut = storageMgr.getVariableValue(this, symbol);
		//System.out.println(" int arayAsstElemRef() Declared Size = " + resOut.valueList.size() );

		
		// bounds check here
		if( index > -1 && index < resOut.arraySize )
		{
			// replace at the index
			resOut.valueList.set(index, resElement);
			//scan.currentToken.printToken();
			if( entry1.dclType == Token.STRING ) // if this is a string assigment
			{
				//resOut.value = ""; // clear it
				StringBuilder strB = new StringBuilder();
				// we need to modify the top level ResultValue's value with the character replacement
				// at a certain index. Since we replaced it in the bottom-level index
				// just reconstruct linearly the top-level string value
				for( int j = 0; j < resOut.arraySize; j++ )
				{
					strB.append( resOut.valueList.get(j).value );
				}
				resOut.value = strB.toString();
			}
			
		}
		else if (index == -1 ) // they want the last populated element
		{
			resOut.valueList.set(resOut.valueList.size()-1, resElement);
		}
		else // out of bounds error
		{
			error("Array indexing out of bounds: Size %s. Found \'%s\'", resOut.arraySize, index);
		}
		
//		// DEBUG print array elements
//		int i = 0;
//		for (ResultValue res : resOut.valueList)
//		{
//			System.out.printf(">>> in asstElemRef() on %s[%s]=%s\n", symbol, i, res.value);
//			i++;
//		}
		
		//increment scanner
		scan.getNext();

		return resOut;
	}
	
	/**
	 * Assumption: the '=' is on the currentToken followed by array operand variable.
	 * @return
	 * @throws Exception
	 */
	private ResultValue preArrayToArrayAssignment( String symbol ) throws Exception
	{
		ResultValue returnRes = new ResultValue();
		ResultValue resOperand = null;
		ResultValue resAssign = null;
		int iLimit = 0;

		resOperand = preExpr(); // have the other array variable
		
		// now retrive from Storage Manager
		resAssign = storageMgr.getVariableValue(this, symbol);
		
		if( resOperand.valueList.size() > resAssign.valueList.size() )
			iLimit = resAssign.arraySize;
		else
			iLimit = resOperand.arraySize;
		
		
		// copy operand array value list into assignee up to its declared size
		for( int i = 0; i < iLimit; i++ )
		{
//			System.out.printf(" in preArrayTOArray(): copying %s[%s]=%s\n"
//			, symbol
//			, i
//			, resOperand.valueList.get(i).value
//			);
			resAssign.valueList.set(i, resOperand.valueList.get(i));
		}
		
		scan.getNext();
		
		return resAssign;
	}
	
	/**
	 * Returns a Result Value that will hold the list of values up to the max
	 * declared size of that array.
	 * Assumptions: the current token is an operand or expression in the list for the array
	 * @return
	 * @throws Exception
	 */
	private ResultValue preArrayAssignmentDeclare(String symbol) throws Exception
	{
		ResultValue returnRes = new ResultValue();
		

		// grab from symbol table before assignment
		STIdentifier var = (STIdentifier) st.getSymbol(symbol);
		returnRes.resultType = var.dclType;

		// go to each operand or expression separated by commas until hit the ; terminator
		// up to the declared array size or if hit max, skip to the ; terminator
		//System.out.println(">>> in preArrayAssignmentDeclare() on size " + declaredArraySize);

		
		
		if( declaredArraySize>0)
		{
			returnRes.arraySize = declaredArraySize;
			//returnRes.resultType = var.dclType;
			int count = declaredArraySize;
			
			
			while( count > 0 )
			{
				ResultValue resElement = preExpr();
				//scan.currentToken.printToken();

				//System.out.println(">>> in preArrayAssignmentDeclare() on element " + resElement.value);

				//resElement = scan.currentToken.toResult();
				//resElement = Utility.toInteger(this, resElement);
				//scan.currentToken.printToken();
				returnRes.valueList.add(resElement);
				// read in a comma or semicolon separator
				scan.getNext();

				if( scan.currentToken.tokenStr.equals(";") )
				{
					//scan.currentToken.printToken();
				
					// what if we havent reached the end of the list size?
					if( count < returnRes.arraySize-1 )
					{
						//int innerCount = count;
						//returnRes.arraySize = count - returnRes.arraySize;
						while( count > 0)
						{
							ResultValue blank = new ResultValue();
							blank.resultType = Token.INTEGER;
							blank.value = "null";
							returnRes.valueList.add( blank );
							//innerCount++;
							count--;
						}
					}

					break;
				}
				if( !scan.currentToken.tokenStr.equals(","))
				{
					errorWithCurrent("Expected comma separation of elements for array assignment");
				}

				
				// preExpr() will do a scan.getNext() and skip the comma
				
				count--;
			}
		}
		else
		{
			while( !scan.currentToken.tokenStr.equals(";") )
			{
//				/scan.currentToken.printToken();

				ResultValue resElement = preExpr();
				returnRes.arraySize++;
				//System.out.println(">>> in preArrayAssignment() on element " + resElement.value);

				//resElement = scan.currentToken.toResult();
				//resElement = Utility.toInteger(this, resElement);
				//scan.currentToken.printToken();
				returnRes.valueList.add(resElement);
				// read in a comma or semicolon separator
				scan.getNext();
				if( scan.currentToken.tokenStr.equals(";") )
				{
					break;
				}
				if( !scan.currentToken.tokenStr.equals(","))
				{
					errorWithCurrent("Expected comma separation of elements for array assignment");
				}
			}

		}

		//scan.currentToken.printToken();
		return returnRes;
	}
	/*
	 * Returns T of the value is in the valueList.
	 * The data type is based on the value to the left of the operand.  
	 */
	private ResultValue preNotInFunc() throws Exception {
		ResultValue returnRes = null;
		//System.out.println("Current token:" + scan.currentToken.tokenStr);
		scan.getNext();
		ResultValue value = storageMgr.getVariableValue(this, scan.currentToken.tokenStr);
		ResultValue valueList  = storageMgr.getVariableValue(this, scan.nextToken.tokenStr);
		//System.out.println("Value : " + value.value + " valueList: " + valueList.valueList);
		returnRes = Utility.notInFunction(this, value, valueList);
		scan.getNext();
		scan.getNext();
		return returnRes;
	}
	/*
	 * Returns T of the value is not in the valueList.
	 * The data type is based on the value to the left of the operand.  
	 */
	private ResultValue preInFunc() throws Exception {
		ResultValue returnRes = null;
		//System.out.println("Current token:" + scan.currentToken.tokenStr);
		scan.getNext();
		ResultValue value = storageMgr.getVariableValue(this, scan.currentToken.tokenStr);
		ResultValue valueList  = storageMgr.getVariableValue(this, scan.nextToken.tokenStr);
		//System.out.println("Value : " + value.value + " valueList: " + valueList.valueList);
		returnRes = Utility.inFunction(this, value, valueList);
		scan.getNext();
		scan.getNext();
		return returnRes;
	}
	
	/**
	 * Assumptions: The 'MAXELEM' function name is on the current token.
	 * @return
	 * @throws Exception
	 */
	private ResultValue preMaxElemFunc() throws Exception
	{
		ResultValue returnRes = null;
		ResultValue resOperand = null;
		
		resOperand = preExpr();
		
		returnRes = Utility.maxElem(this, resOperand);
		
		scan.getNext();
		
		return returnRes;
		
	}
	
	/**
	 * Assumptions: The 'ELEM' function name is on the current token
	 * @return
	 * @throws Exception
	 */
	private ResultValue preElemFunc() throws Exception
	{
		ResultValue returnRes = null;
		ResultValue resOperand = null;
		
		resOperand = preExpr();
		
		returnRes = Utility.elem(this, resOperand);
		
		scan.getNext();
		
		return returnRes;
	}
	
	/**
	 * Assumptions:
	 * <br> "SPACES" is on the current token
	 * <br> We have a Utility.spaces()
	 * <br> Purpose:
	 * <br>Calls the SPACES function from Utility
	 */
	private ResultValue preSpacesFunc() throws Exception
	{
		ResultValue returnRes = null;
		ResultValue resOperand = null;
		// Check for 'SPACES' token word and maybe also token type
		if( !scan.currentToken.tokenStr.equals("SPACES"))
			errorWithCurrent("Expected 'SPACES' keyword for built-in");
		
		// the next parameter better be an Operand of STRING type
		//scan.getNext();
		//scan.currentToken.printToken();

		resOperand = preExpr();
		returnRes = Utility.spaces(this, resOperand);
		
		scan.getNext(); // go past the operand, sorry
		
		return returnRes;
	}
	
	/**
	 * Calls the length function from Utilty
	 * Assumptions: Assumes the current token is the token 'LENGTH'
	 * @return
	 * @throws Exception
	 */
	private ResultValue preLengthFunc() throws Exception
	{
		ResultValue returnRes = null;
		ResultValue resArrayOp = null;
		
		// Check for 'LENGTH' token word and maybe also token type
		if( !scan.currentToken.tokenStr.equals("LENGTH"))
			errorWithCurrent("Expected 'LENGTH' keyword for built-in");
		//System.out.println("In preLengthFunc()");

		// the next token is a parameter that should be the symbol
		// for an array var
		resArrayOp = preExpr();
		//scan.currentToken.printToken();
		
		// call Utility.length to convert for returnRes
		returnRes = Utility.length(this, resArrayOp);
		
		scan.getNext();
		
		return returnRes;
	}
	
	private ResultValue preStringConcat() throws Exception
	{
		ResultValue res = preExpr();
		ResultValue resNextOp = preExpr();
		
		//System.out.printf("\t\tDEBUG: Inside preStringConcat(): op1=%s op2=%s\n", res.value, resNextOp.value);

		if ( scan.getNext().equals(")") == false )
			error("Expected \')\' after operands for \'%s\' operator", "#");
		
		// convert
		ResultValue returnRes = Utility.concatenate( this, res, resNextOp );
		//System.out.println(" # RETURN RESULT VAL: " + returnRes.value + " Type: " + returnRes.resultType);
		
		// return ResultValue
		return returnRes;
	}

	private ResultValue preNotEqualTo() throws Exception{
		ResultValue res = preExpr();
		ResultValue resNextOp = preExpr();
		//System.out.printf("\t\tDEBUG: Inside preExponent(): op1=%s op2=%s\n", res.value, resNextOp.value);
		
		if ( scan.getNext().equals(")") == false )
			error("Expected \')\' after operands for \'%s\' operator", "!=");

		
		// convert
		ResultValue returnRes = Utility.notEqual(this, res, resNextOp );

		//System.out.println(" == RETURN RESULT VAL: " + returnRes.value );

		return returnRes; // TODO change this into returnRes
	}

	private ResultValue preLesserOrEquals() throws Exception {
		ResultValue res = preExpr();
		ResultValue resNextOp = preExpr();
		//System.out.printf("\t\tDEBUG: Inside preExponent(): op1=%s op2=%s\n", res.value, resNextOp.value);
		
		if ( scan.getNext().equals(")") == false )
			error("Expected \')\' after operands for \'%s\' operator", "<=");

		
		// convert
		ResultValue returnRes = Utility.lessThanEqual(this, res, resNextOp );

		//System.out.println(" == RETURN RESULT VAL: " + returnRes.value );

		return returnRes; // TODO change this into returnRes
	}

	private ResultValue preGreaterThan() throws Exception {
		ResultValue res = preExpr();
		ResultValue resNextOp = preExpr();
		//System.out.printf("\t\tDEBUG: Inside preExponent(): op1=%s op2=%s\n", res.value, resNextOp.value);
		
		if ( scan.getNext().equals(")") == false )
			error("Expected \')\' after operands for \'%s\' operator", ">");

		
		// convert
		ResultValue returnRes = Utility.greaterThan(this, res, resNextOp );

		//System.out.println(" == RETURN RESULT VAL: " + returnRes.value );

		return returnRes; // TODO change this into returnRes
	}

	private ResultValue preLessThan() throws Exception{
		ResultValue res = preExpr();
		ResultValue resNextOp = preExpr();
		//System.out.printf("\t\tDEBUG: Inside preExponent(): op1=%s op2=%s\n", res.value, resNextOp.value);
		
		if ( scan.getNext().equals(")") == false )
			error("Expected \')\' after operands for \'%s\' operator", "<");

		
		// convert
		ResultValue returnRes = Utility.lessThan(this, res, resNextOp );

		//System.out.println(" == RETURN RESULT VAL: " + returnRes.value );

		return returnRes; // TODO change this into returnRes
	}

	ResultValue preEqualTo() throws Exception
	{
		ResultValue res = preExpr();
		ResultValue resNextOp = preExpr();
		//System.out.printf("\t\tDEBUG: Inside preEqualTo(): op1=%s op2=%s\n", res.value, resNextOp.value);
		
		scan.getNext();
		//scan.currentToken.printToken();
		if ( scan.currentToken.tokenStr.equals(")") == false )
			error("Expected \')\' after operands for \'%s\' operator", "==");

		
		// convert
		ResultValue returnRes = Utility.equal(this, res, resNextOp );

		//System.out.println(" == RETURN RESULT VAL: " + returnRes.value );

		return returnRes; // TODO change this into returnRes
	}
	private ResultValue preGreatEqualTo() throws Exception 
	{
		ResultValue res = preExpr();
		ResultValue resNextOp = preExpr();
		//System.out.printf("\t\tDEBUG: Inside preExponent(): op1=%s op2=%s\n", res.value, resNextOp.value);
		
		if ( scan.getNext().equals(")") == false )
			error("Expected \')\' after operands for \'%s\' operator", ">=");

		
		// convert
		ResultValue returnRes = Utility.greaterThanEqual(this, res, resNextOp );

		//System.out.println(" == RETURN RESULT VAL: " + returnRes.value );

		return returnRes; // TODO change this into returnRes
	}

	// performs perfix exponent function
	// Assumption: Operator '^' is on the current token
	ResultValue preExponent() throws Exception
	{
		ResultValue res = preExpr();
		ResultValue resNextOp = preExpr();
		//System.out.printf("\t\tDEBUG: Inside preExponent(): op1=%s op2=%s\n", res.value, resNextOp.value);
		
		if ( scan.getNext().equals(")") == false )
			error("Expected \')\' after operands for \'%s\' operator", "^");

		
		// convert
		ResultValue returnRes = Utility.exponent(this, res, resNextOp );

		
		return returnRes; // TODO change this into returnRes
	}
	
	ResultValue preMinus() throws Exception
	{
		ResultValue res = preExpr();
		ResultValue resNextOp = preExpr();
		//System.out.printf("\t\tDEBUG: Inside preExponent(): op1=%s op2=%s\n", res.value, resNextOp.value);

		
		if ( scan.getNext().equals(")") == false )
			error("Expected \')\' after operands for \'%s\' operator", "-");

		
		// convert
		ResultValue returnRes = Utility.minus(this, res, resNextOp );

		
		return returnRes; // TODO change this into returnRes
	}
	
	// Interp prefix function or operator call
	// Assumption:  operator is on the current token
	ResultValue preBinaryNumOp() throws Exception
	{
	    String operatorStr = scan.currentToken.tokenStr;
	    ResultValue res = preExpr(); // get the 1st operand which can be an expr
	    ResultValue resNextOp = preExpr(); // get the 2nd operand which can be an expr
		//System.out.printf("\t\tDEBUG: Inside preBinaryNumOp(): op1=%s op2=%s\n", res.value, resNextOp.value);


		
	    switch (operatorStr){
	    	case "/": // TODO
	    		//scan.currentToken.printToken();
	    		if ( scan.getNext().equals(")") == false )
	    			error("Expected \')\' after operands for \'%s\' operator", "/");
	            res = Utility.divide(this, res, resNextOp );
        		break;
	    	case "*": 
	    		if ( scan.getNext().equals(")") == false )
	    			error("Expected \')\' after operands for \'%s\' operator", "*");
	            res = Utility.multiply( this, res, resNextOp );
	    		break;
	        default:
	            error ("unknown numeric operator, found '%s'"
	                , operatorStr);
	    }
	    // for consistency, what do we need to do?
	    // TODO ??'
	    return res; 
	}

	// Interp prefix add
	// Assumption:  operator is in current token
	// Add can have a variable number of arguments
	ResultValue preAdd() throws Exception
	{
		ResultValue res = preExpr();
		ResultValue resNextOp = preExpr();
		
		if ( scan.getNext().equals(")") == false )
			error("Expected \')\' after operands for \'%s\' operator", "+");

		
		// convert
		
		ResultValue returnRes = Utility.add(this, res, resNextOp ); 

		
		return returnRes; // TODO change this into returnRes
	}
	
	ResultValue preNot() throws Exception
	{
		ResultValue resOperand = preExpr();
		ResultValue returnRes = null;
		//ResultValue resNextOp = preExpr();
		
		//scan.currentToken.printToken();
		
		returnRes = Utility.not(this, resOperand );
		
		scan.getNext();
		return returnRes;
	}
	
	ResultValue preAnd() throws Exception
	{
		ResultValue resOperand = preExpr();
		ResultValue resNextOp = preExpr();
		ResultValue returnRes = null;
		//ResultValue resNextOp = preExpr();
		
		returnRes = Utility.and(this, resOperand, resNextOp );
		
		scan.getNext();
		
		if ( scan.currentToken.tokenStr.equals(")") == false )
			error("Expected \')\' after operands for \'%s\' operator", "and");
		//scan.currentToken.printToken();

		return returnRes;
	}
	
	ResultValue preOr() throws Exception
	{
		ResultValue resOperand = preExpr();
		ResultValue resNextOp = preExpr();
		ResultValue returnRes = null;
		//ResultValue resNextOp = preExpr();
		
		returnRes = Utility.or(this, resOperand, resNextOp );
		
		scan.getNext();
		
		if ( scan.currentToken.tokenStr.equals(")") == false )
			error("Expected \')\' after operands for \'%s\' operator", "or");
		//scan.currentToken.printToken();

		return returnRes;
	}
	
	
	/**
	 * Error handling routine that takes in a string format, the variable number of args
	 * to compliment the string format, and throws a new ParserException with the diagnostic text,
	 * line number, and sourceFileNm of interest.
	 * @param fmt	the specified format for the output message.
	 * @param varArgs the variable number of arguments to that match the string format symbols
	 * @throws Exception the parser error
	 */
	public void error( String fmt, Object... varArgs ) throws Exception
	{
		String diagnosticTxt = String.format( fmt, varArgs);
		//System.out.println("iColPos: " + scan.currentToken.iColPos);
		throw new ParserException( scan.currentToken.iSourceLineNr+1
									, diagnosticTxt
									, this.sourceFileNm );
		
	}
	
	/**
	 * Catches parsing errors on Scanner current token cases.
	 * @param message The error message for this current token.
	 * @throws Exception The parse error involving the current token.
	 */
	public void errorWithCurrent( String message ) throws Exception
	{
		String diagnosticTxt = String.format( "%s, found \'%s\' ", message, scan.currentToken.tokenStr);
		throw new ParserException( scan.currentToken.iSourceLineNr+1
									, diagnosticTxt
									, this.sourceFileNm );
	}
}
