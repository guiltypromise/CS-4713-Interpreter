package parser;

import java.util.ArrayList;

public class ResultValue implements Cloneable {

	// type - usually data type of the result
	public int resultType; // the result type adhering to the Token classification constants
	
	// value - the value of the result
	public String value;
	
	// structure - is it primitive? fixed array? unbounded array?
	public int structure;
	
	// terminatingStr - used for end of lists of things 
	 				// ( a list of stmts might be terminated by "endwhile" )
	public String terminatingStr;
	
	public int arraySize;
	
	//Should a ResultValue be able to hold ArrayList of itself for Array variables?
	public ArrayList <ResultValue> valueList;
	

	/**
	 * Constructs and initializes an empty ResultValue.
	 */
	public ResultValue()
	{
		resultType = 0;
		value = "";
		//structure = "";
		structure = 0;
		terminatingStr = "";
		arraySize = 0;
		valueList = new ArrayList<ResultValue>(0);
	}
	
	public ResultValue clone() throws CloneNotSupportedException
	{
		return (ResultValue) super.clone();
	}
}

//Int arr[] = 5, 20, 30;
//Int arr2[] = arr;
//
//arr2[0] = 5;
