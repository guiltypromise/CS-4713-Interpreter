/**
 * Scanner class - PGM 2
 * @author Jason Martin
 * 
 * TO do list:
 * [ ] combine 2 character operators
 * [ ]
 * 
 * Sample outputs met:
 * [X] sample_output.txt
 * [X] first error case ( line 7 float is not valid format )
 * [x] second error case ( line 19 has unfinished string literal )
 * [X] third error case ( line 7 the float has a non-number in it )
 * 
 */

package scanner;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.StringTokenizer;

import symboltable.STControl;
import symboltable.STEntry;
import symboltable.STFunction;
import symboltable.SymbolTable;
import token.Token;

public class Scanner 
{
	public Token currentToken;
	
	public Token nextToken;
	
	public String sourceFileNm;
	
	private SymbolTable symbolTable;
	
	private final static String delimiters = 
			" \t;:()\'\"=!<>+-*/[]#,^\n"; // terminate a token
	
	private BufferedReader reader;
	
	private File file;
	
	private char[] textCharM;
	
	private ArrayList<String> sourceLineM;
	
	public int iSourceLineNr;
	
	private int iColPos;
	
	private static boolean combinedToken;
	
	private int iPairsCount;

	/**
	 * Constructs a custom Scanner that loads the input file
	 * and the given symbol table and sets up the input stream
	 * for processing.
	 * @param sourceFileNm The source file name.
	 * @param symbolTable The symbol table.
	 */
	public Scanner(String sourceFileNm, SymbolTable symbolTable ) 
	{
		this.sourceFileNm = sourceFileNm;
		this.symbolTable = symbolTable;
		file = new File( sourceFileNm );
		String line = "";
		sourceLineM = new ArrayList<String>();
		
		try // accessing source file and grabbing all the input lines
		{ 
			FileReader fileReader = new FileReader( sourceFileNm );
			reader = new BufferedReader( fileReader );
			while(( line = reader.readLine()) != null )
			{
				sourceLineM.add(line); // store the input line to our Scanner's memory
			}
			//System.out.println( sourceLineM.toString());
			
			iSourceLineNr = 0;
			iColPos = 0;
			textCharM = sourceLineM.get(iSourceLineNr).toCharArray();
			currentToken = new Token();
			nextToken = new Token();
			
			// get the next token?
			this.getNext();
			
			combinedToken = false;
			iPairsCount = 0;
			
			reader.close(); // close the file input source
		} 
		catch (FileNotFoundException e) // no file found
		{
			//e.printStackTrace();
			System.out.println("ERROR: '" + sourceFileNm + "' cannot be found");
		} 
		catch (IOException e) // IO error occurred
		{
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	/**
	 * Gets the next valid token.
	 * @return A string for the next token to be processed.
	 * @throws Exception if invalid formatting on a token occurs.
	 */
	public String getNext() throws Exception
	{
		if( combinedToken )
		{
			combinedToken = false;
			getNext();
		}
		
		// copy nextToken into currentToken
		currentToken.tokenStr = nextToken.tokenStr;
		currentToken.primClassif = nextToken.primClassif;
		currentToken.subClassif = nextToken.subClassif;	
		currentToken.iSourceLineNr = nextToken.iSourceLineNr;
		currentToken.iColPos = nextToken.iColPos;
		
		//currentToken.printToken();
		
		// the below isnt good since if we skip more than
		if( iColPos >= textCharM.length )  // if we're going to expect a new line
		{
			nextToken = new Token();
			iSourceLineNr++;
			if( iSourceLineNr < sourceLineM.size() )
				textCharM = sourceLineM.get(iSourceLineNr).toCharArray(); // get a new source line from memory
			else // reached End of File
			{
				//currentToken.primClassif = Token.EOF;
				nextToken.primClassif = Token.EOF;
				return "";
			}
			this.iColPos = 0;
		}

		int iBeginToken = iColPos;
		int iEndToken = iColPos;
		boolean bFoundToken = false;
		boolean bFoundEmptyLine = false;
		boolean bFoundString = false;
		boolean bFoundSingleQuote = false;
		boolean bFoundDoubleQuote = false;
		int iLiteralMarkersCount = 0;
		iPairsCount = 0;
		int iDoubleQuoteCount = 0;
		int iSingleQuoteCount = 0;
		//int iCharsCounted = 0;

		if( iColPos <= 0 )
		{
			//iSourceLineNr++;
			// TODO add debug control here
			//System.out.printf("%3d %s\n", iSourceLineNr+1, sourceLineM.get(iSourceLineNr));
			
			
		}
		// traverse through the characters of the source line char array, 
		// starting at the last known column position to try and grab a "token".
		do 
		{
			while( iColPos < textCharM.length ) // 
			{
				if( "/".indexOf( textCharM[iColPos] ) > -1 )
				{
					if ("/".indexOf( textCharM[iColPos+1] ) > -1 ) 
					{
						//System.out.println(" >>>>>>> Found Comment");
						
						// Skip this comment end of the line
						iColPos = textCharM.length;
						iBeginToken = iColPos;
						iEndToken = iColPos;
						break;
					}
				}
				if( delimiters.indexOf( textCharM[iColPos] ) == -1 || bFoundString) // not a delimiting char
				{
					//System.out.println("Found Non-delimiter");

					bFoundToken = true;
					iEndToken++;
					if( "\"".indexOf( textCharM[iColPos]) != -1 ) 
					{
						//System.out.println(" >>>>> Found END STRING " + iLiteralMarkersCount);
						if( bFoundDoubleQuote )
						{
							// check for escape char
							if( "\\".indexOf(textCharM[iColPos-1]) < 0 ) // if not found
							{
								iLiteralMarkersCount++;
								bFoundDoubleQuote = false;
								iEndToken--;
								iColPos++;
								break;
							}
						}
					}
					else if ( "\'".indexOf(textCharM[iColPos]) != -1 ) 
					{
						if( bFoundSingleQuote )
						{
							// check for escape char
							if( "\\".indexOf(textCharM[iColPos-1]) < 0 ) // if not found
							{
								bFoundSingleQuote = false;
								iLiteralMarkersCount++;
								//iSingleQuoteCount++;
								iEndToken--;
								iColPos++;
								break;
							}
						}
					}
				}
				else // it is a delimiting char, take it for classifying
				{
					// could this start a string literal?
					if( "\'\"".indexOf( textCharM[iColPos]) != -1 ) 
					{
						//System.out.println(" >>>>> Found STRING");
						if( bFoundString != true )
						{
							if( "\'".indexOf( textCharM[iColPos]) != -1 )
							{
								bFoundSingleQuote = true;
								//iSingleQuoteCount++;
							}
							else if( "\"".indexOf( textCharM[iColPos]) != -1 )
							{
								bFoundDoubleQuote = true;
							}
							
							// check for escape char
							if( "\\".indexOf(textCharM[iColPos-1]) < 0 ) // if not found, we check out
							{
								iLiteralMarkersCount++;
								//iDoubleQuoteCount++;
								iBeginToken++;
								bFoundString = true;
								iEndToken++;
							} // else it was an attempt to escape the ' or "
						}
					}
					else if(iBeginToken == iColPos && iEndToken == iColPos)
					{
						if( "(),:;[]+-*/<>=!#^".indexOf( textCharM[iColPos]) != -1)
						{	
							if( "()[]".indexOf( textCharM[iColPos]) != -1)
							{
								iPairsCount++;
								//System.out.println(" >>>>> Found ()[] " + iPairsCount);
							}
							iEndToken++;
							iColPos++;

							break;
						} 
						else // its whitespace, move token markers past the whitespace
						{
							iBeginToken++;
							iEndToken++;
						}
					}
					else
					{
						break;
					}

					
				} // end decide if-else delimiter check
				
				iColPos++; // keep traversing until we can get a token beginning	

			} // end traverse while engine
			
			// did we go through a whole line without getting a token?
			if( iBeginToken == iColPos && iEndToken == iColPos )
			{
				bFoundEmptyLine = true;
				//currentToken = new Token();
				nextToken = new Token();
				iBeginToken = 0;
				iEndToken = 0;
				iSourceLineNr++;
				if( iSourceLineNr < sourceLineM.size() )
					textCharM = sourceLineM.get(iSourceLineNr).toCharArray(); // get a new source line from memory
				else // reached End of File
				{
					//currentToken.primClassif = Token.EOF;
					nextToken.primClassif = Token.EOF;
					return "";
				}
				//System.out.println("  " + (iSourceLineNr+1) + " " + sourceLineM.get(iSourceLineNr) );
				
				
				// TODO add debug control around here
				//System.out.printf("%3d %s\n", iSourceLineNr+1, sourceLineM.get(iSourceLineNr));
				
				
				
				iColPos = 0;
			}
			else // it wasnt empty, get out
			{
				break;
			}
		} while ( bFoundEmptyLine ) ; // end read line
		
		// Set token position attributes
		nextToken.iSourceLineNr = this.iSourceLineNr ;
		nextToken.iColPos = this.iColPos ;
		//currentToken.iSourceLineNr = this.iSourceLineNr;
		//currentToken.iColPos = this.iColPos;

		/* --------------- begin classification steps --------------------- */
		
		// save the token into 
		//currentToken.tokenStr = sourceLineM.get( iSourceLineNr ).substring(iBeginToken
		//																  , iEndToken);
		nextToken.tokenStr = sourceLineM.get( iSourceLineNr ).substring(iBeginToken
				  , iEndToken);
		nextToken.iSourceLineNr = this.iSourceLineNr;
		nextToken.iColPos = this.iColPos;
		//System.out.println(" >>>>>> Unpaired single quote? " + bFoundSingleQuote);

		// check symbol table for re-classification
		//STEntry stEntry = symbolTable.getSymbol( currentToken.tokenStr );
		STEntry stEntry = symbolTable.getSymbol( nextToken.tokenStr );
		if( stEntry != null && !bFoundString) // if it exists in the symbol table already
		{
			//currentToken.primClassif = stEntry.primClassif;
			nextToken.primClassif = stEntry.primClassif;
			if( stEntry.primClassif == Token.FUNCTION )
			{
				//STFunction stFunc = (STFunction) symbolTable.getSymbol( currentToken.tokenStr );
				STFunction stFunc = (STFunction) symbolTable.getSymbol( nextToken.tokenStr );

				//currentToken.subClassif = stFunc.definedBy;
				nextToken.subClassif = stFunc.definedBy;
			}
			else if( stEntry.primClassif == Token.CONTROL )
			{
				//STControl stControl = (STControl) symbolTable.getSymbol( currentToken.tokenStr );
				STControl stControl = (STControl) symbolTable.getSymbol( nextToken.tokenStr );

				nextToken.subClassif = stControl.subClassif;
				//currentToken.subClassif = stControl.subClassif;

			}
			else if( stEntry.primClassif == Token.OPERAND ) 
			{
				//System.out.println("FOUND OPERAND");
				nextToken.subClassif = Token.IDENTIFIER;
			}
			//return currentToken.tokenStr;
			return nextToken.tokenStr;
		}
		
		if( Character.isLetterOrDigit(textCharM[iBeginToken]) 
				//|| "\\".indexOf(textCharM[iBeginToken] ) > -1)  
				|| bFoundString )
			// this could be an operand or string, or string literal beginning with escape char like \t
		{
			/*
			if( Character.isUpperCase(textCharM[iBeginToken]) ) // typo'd declaration statement that wouldve been operand??
			{
				//System.out.println(" >>>>>> FOUND POSSIBLE DECLARARITON " + nextToken.tokenStr);
				nextToken.primClassif = Token.CONTROL;
				nextToken.subClassif = Token.DECLARE;
				return currentToken.tokenStr;
			}
			*/
			
			//System.out.println(" >>>>>> FOUND OPERAND " + nextToken.tokenStr);
			//currentToken.primClassif = Token.OPERAND;
			nextToken.primClassif = Token.OPERAND;
			
			if( bFoundString )
			{
				if( iLiteralMarkersCount < 2 )//|| iDoubleQuoteCount % 2 != 0 || iSingleQuoteCount % 2 != 0 )
				{
					throw new Exception("Line= " 
							+ (nextToken.iSourceLineNr)
							//+ " Pos= " + (iColPos+1)
							+ ": Missing string terminator");

				}
				//currentToken.subClassif = Token.STRING;
				nextToken.subClassif = Token.STRING;
				// we have a string, now convert escaped value to hex values
				// for each "character" in the string literal
				
				// prepare the string into a char array
				char [] chArray = nextToken.tokenStr.toCharArray();
				String tokenStr = "";
				
				//System.out.println("\n >>> TOKEN: " + nextToken.tokenStr);
				
				for( int i = 0; i < chArray.length; i++ )
				{
					// if we found the start of an escaped character
					if( "\\".indexOf( chArray[i] ) > -1  )
					{
						i++;
						//System.out.println("Found escaped at " + i);
						
						// check the next char byte for substitution
						if( "\"".indexOf(chArray[i]) > -1 ) // double quote
						{
							tokenStr += String.valueOf(  chArray[i]);
						}
						else if( "\'".indexOf(chArray[i]) > -1 ) // apostrophe, single quote
						{
							tokenStr += String.valueOf(  chArray[i] );
						}
						else if( "\\".indexOf(chArray[i]) > -1 ) // escaped back slash
						{
							tokenStr += String.valueOf(  chArray[i] );
						} 
						else if( "t".indexOf(chArray[i]) > -1 ) // tab
						{
							//System.out.println("Replace '\\t' at " + i);

							chArray[i] = 0x09;
							tokenStr += String.valueOf(  chArray[i] );
						}
						else if( "n".indexOf(chArray[i]) > -1 ) // line feed
						{
							//System.out.println("Replace '\\n' at " + i)
							chArray[i] = 0x0A;
							tokenStr += String.valueOf(  chArray[i] );
						}
						
						else if( "a".indexOf(chArray[i]) > -1 ) // alarm bell
						{
							chArray[i] = 0x07;
							tokenStr += String.valueOf(  chArray[i] );
						}
						else // unrecognized escape char?
						{
							throw new Exception("Line= " 
									+ (nextToken.iSourceLineNr )
									//+ " Pos= " + (nextToken.i+1)
									+ ": Invalid escaped character \'" + chArray[i-1] + chArray[i] + "\' ");
						}
					}
					else // add the char back into temp string array
					{
						tokenStr += String.valueOf( chArray[i] );
					}
					//System.out.println(  tokenStr );
				}
				
				
				// TODO add debug control here too
				//nextToken.tokenStr = hexPrint( 25, tokenStr );
				
			}
			else if( Character.isLetter( textCharM[iBeginToken] )) // check if identifier type
			{
					//System.out.println(">>> FOUND IDENTIFIER " + currentToken.tokenStr);

					//currentToken.subClassif = Token.IDENTIFIER;
					nextToken.subClassif = Token.IDENTIFIER;
					if ( "TF".indexOf(textCharM[iBeginToken]) >- 1) // is this a boolean constant instead?
					{
						nextToken.subClassif = Token.BOOLEAN;
					}
			}
			else // its a numeric constant or a float
			{
				// keep going to check if it has ONE decimal then a number behind it
				// have to also make sure we're getting numbers only
				//currentToken.subClassif = Token.INTEGER; 
				nextToken.subClassif = Token.INTEGER; 
				boolean decimalCheck = false;
				for( int i = iBeginToken; i < iEndToken; i++ )
				{
					if( ".".indexOf( textCharM[i] ) != -1 )
					{
						if( decimalCheck ) // we have a decimal before this one?
						{
							throw new Exception("Line= " 
									+ (nextToken.iSourceLineNr )
									//+ " Pos= " + (iColPos+1)
									+ ": Invalid float format");
						}
						decimalCheck = true;
					}
					else if (! Character.isDigit( textCharM[i] ) )
					{
						throw new Exception("Line= " 
								+ (nextToken.iSourceLineNr )
								//+ " Pos= " + (iColPos+1)
								+ ": Non-numeric character found in numeric value");
					}
				}
				if( decimalCheck )
				{
					//currentToken.subClassif = Token.FLOAT;
					nextToken.subClassif = Token.FLOAT;
				}
			} // end else check numeric/float
		}
		else // its some kind of operator or separator, possibly char that is fake or illegal use of escaped slash?
		{
			//System.out.println(" Current begin token: " + textCharM[iBeginToken]);
			//System.out.println(" >>>>>> FOUND Op/Sep " + nextToken.tokenStr);

			// lets check if its a separator
			if( "(),:;[]".indexOf( textCharM[iBeginToken] ) != -1)
			{
				//currentToken.primClassif = Token.SEPARATOR;
				nextToken.primClassif = Token.SEPARATOR;			
			}
			// check if its an operator
			else if( "+-*/<>=!#^".indexOf( textCharM[iBeginToken] ) != -1 )
			{
				//currentToken.primClassif = Token.OPERATOR;
				nextToken.primClassif = Token.OPERATOR;
			}
			//if(currentToken.primClassif < 1  ) // there's an error now
			//if(nextToken.primClassif < 1  ) // there's an error now
			else if("\\".indexOf( textCharM[iBeginToken] ) != -1) // escaped slash when not in string?
			{
				throw new Exception("Line= " 
						+ (nextToken.iSourceLineNr )
						//+ " Pos= " + (iColPos+1)
						+ ": Invalid use of escaped character \'" + nextToken.tokenStr + "\' ");
			}
			else
			{
				throw new Exception("Line= " 
						+ (nextToken.iSourceLineNr )
						//+ " Pos= " + (iColPos+1)
						+ ": Syntax error \'" + nextToken.tokenStr + "\' ");
			}
		}	
		
		// Recognize when we can combine operator tokens for those assignment operators
		if( "<>!=".indexOf(currentToken.tokenStr) > -1 && !bFoundString ) // also make sure they're not part of a string literal
		{
			if( "=".indexOf(nextToken.tokenStr) >- 1)
			{
				combinedToken = true;
				currentToken.tokenStr += nextToken.tokenStr;
				//System.out.println(" \t\t\t\t\t\t\t>>>>> Combining for " + currentToken.tokenStr + nextToken.tokenStr);
			}
			/*
			else // invalid assignment operator?
			{
				throw new Exception("Line= " 
						+ (nextToken.iSourceLineNr )
						//+ " Pos= " + (iColPos+1)
						+ ": Invalid assignment operator \'" + currentToken.tokenStr + nextToken.tokenStr + "\' ");
			}*/
		}
		
		
//		// by now nextToken has been set, check the operand/identifer for array []'s
//		if( currentToken.primClassif == Token.OPERAND )
//		{
//			if( currentToken.subClassif == Token.IDENTIFIER )
//			{
//				if( nextToken.tokenStr.equals("["))
//				{
//					//System.out.println(">>> FOUND [");
//					//nextToken.primClassif = Token.CONTROL;
//				}
//			}
//		}

		return currentToken.tokenStr; // return the token
		//return nextToken.tokenStr; // return the token
	}
	

	
	
	
    /**
     * Prints a string that may contain non-printable characters as two lines.  
     * <p>
     * On the first line, it prints printable characters by simply
     * printing the character.  For non-printable characters
     * in the string, it prints ". ".  
     * <p>
     * The second line prints a two character hex value for the non printable
     * characters in the string line.  For the printable characters, it prints 
     * a space.
     * <p>
     * It is sometimes necessary to print the first line on the end of
     * an existing line of output.  This would make it difficult to properly 
     * align the second line of output.  The indent parameter is for indenting 
     * the second line.
     * <p><blockquote><pre>
     * Example for the string "\tTX\tTexas\n"
     *      . TX. Texas.
     *      09  09     0A
     * </pre></blockquote><p>    
     * @param indent  the number of spaces to indent the second printed line
     * @param str     the string to print which may contain non-printable characters
    
    */

    public String hexPrint(int indent, String str)
    {
        int len = str.length();
        char [] charray = str.toCharArray();
        char ch;
        String output = "";
        
        // print each character in the string
        for (int i = 0; i < len; i++)
        {
            ch = charray[i];
            if (ch > 31 && ch < 127)   // ASCII printable characters
            {
            	//System.out.print("Printable\n");
               // System.out.printf("%c\n", ch);
            	output += String.format( "%c", ch );
            }
            else
            {
            	//System.out.print("NOT\n");

                //System.out.printf(". ");
            	output += ". ";
            }
        }
        output += "\n";
        //System.out.printf("\n");
        // indent the second line to the number of specified spaces
        for (int i = 0; i < indent; i++)
        {
            //System.out.printf(" ");
            output += " ";
        }
        // print the second line.  Non-printable characters will be shown
        // as their hex value.  Printable will simply be a space
        for (int i = 0; i < len; i++)
        {
            ch = charray[i];
            // only deal with the printable characters
            if (ch > 31 && ch < 127)   // ASCII printable characters
            {
                //System.out.printf(" ", ch);
            	output += String.format( " ", ch );
            }
            else
            {
                //System.out.printf("%02X", (int) ch);
            	output += String.format( "%02X", (int) ch);
            }
        }    
        //System.out.printf("\n");
        //output += "\n";
        return output;
    }

	/**
	 * Sets the position of the Scanner's current token to the line number and column position in the
	 * source file of the inputted token.
	 * @param token The token that contains a source line number and a column position
	 * 				to set the Scanner for reading.
	 */
	public void setPosition( int iSourceLineNr, int iColPos )
	{
		//System.out.printf("\t\tDEBUG: Inside setPos(): saved position is %s, %s\n", 
		//		iSourceLineNr+1, iColPos+1 );
		//System.out.printf("\t\tDEBUG: Inside setPos(): current position is %s\n", 
		//		this.iSourceLineNr+1 );

		// set the position to this line and col pos
		this.iColPos = iColPos;
		this.iSourceLineNr = iSourceLineNr;
		//System.out.printf("\t\tDEBUG: Inside setPos(): updated position is %s, %s\n"
		//		, this.iSourceLineNr+1
		//		, this.iColPos+1 );
		
		// initialize textCharM
		this.textCharM = sourceLineM.get(iSourceLineNr).toCharArray();
		
		// make current and next token new objects
		this.currentToken = new Token();
		this.nextToken = new Token();
		
		//get the first token into nextToken
		try {
			this.getNext();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
