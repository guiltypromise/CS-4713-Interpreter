package utility;

import parser.Parser;
import parser.ResultValue;
import token.Token;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;


public final class Date 
{
	
	public static String birthDt(int val) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MMM-dd");
		//String date = sdf.parse(String.valueOf(val));
		String date = null;
		return date;
	}
	
	public static ResultValue dateDiff(Parser parser, ResultValue res1, ResultValue res2) {
		ResultValue resOut = new ResultValue();
		int resDays = 0;
		String date1 = res1.value;
		String date2 = res2.value;
		//separate and convert dates into years, months, and days.
		int year1 = Integer.parseInt(date1.substring(0, 4));
		int year2 = Integer.parseInt(date2.substring(0, 4));
		int month1 = Integer.parseInt(date1.substring(5, 7));
		int month2 = Integer.parseInt(date2.substring(5, 7));
		int days1 = Integer.parseInt(date1.substring(8, 10));
		int days2 = Integer.parseInt(date2.substring(8, 10));
	//	System.out.print(year1 + " " + year2 + " " + month1 + " " + month2 
	//			+ " " + days1 + " " + days2);
		
		//subtract day2 from day1, month2 from month1, and year2 from year1
		resDays = days1-days2;
		int resYears = year1-year2;
		int resMonths = month1-month2;
	//	System.out.print(resDays + " " + resMonths + " " + resYears);
		//multiply years by 365, add to days.
		resDays = resDays+(resYears*365);		
		//convert months into days. Some months have more/less days, take into account.
		switch(resMonths){
			case 0: //less than a month
				break;
			case 1: //one month
				resDays+=31;
				break;
			case 2: //two months, and etc.
				resDays+=59;
				break;
			case 3: 
				resDays+=90;
				break;
			case 4: 
				resDays+=120;
				break;
			case 5: 
				resDays+=151;
				break;
			case 6: 
				resDays+=181;
				break;
			case 7: 
				resDays+=212;
				break;
			case 8: 
				resDays+=243;
				break;
			case 9: 
				resDays+=273;
				break;
			case 10: 
				resDays+=304;
				break;
			case 11: 
				resDays+=334;
				break;
			case 12: //shouldn't really be reached, since 12 months would be a year.
				//kept anyway, just in case.
				resDays+=365;
		}
		resOut.value = String.format("%s", resDays);
		resOut.resultType = Token.INTEGER;
		return resOut;
	}
	
	public static ResultValue dateAdj(ResultValue res, ResultValue resDays) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		ResultValue resOut = new ResultValue();
		String date = res.value;
		int days = Integer.parseInt(resDays.value);
		
		//separate date into years, months, days.
		int dYear = Integer.parseInt(date.substring(0, 4));
		int dMonth = Integer.parseInt(date.substring(5, 7));
		int dDays = Integer.parseInt(date.substring(9, 10));
		
		Calendar calendar = new GregorianCalendar(dYear, dMonth-1, dDays);
		calendar.add(Calendar.DAY_OF_MONTH, days);
		
		resOut.value = sdf.format(calendar.getTime());
		resOut.resultType = Token.DATE;
		return resOut;
	}
	
	public static ResultValue dateAge(ResultValue res1, ResultValue res2) {
		ResultValue resOut = new ResultValue();
		String date1 = res1.value;
		String date2 = res2.value;
		//separate dates into years, months, days. Same as dateDiff here.
		int year1 = Integer.parseInt(date1.substring(0, 4));
		int year2 = Integer.parseInt(date2.substring(0, 4));
		int month1 = Integer.parseInt(date1.substring(5, 7));
		int month2 = Integer.parseInt(date2.substring(5, 7));
		int days1 = Integer.parseInt(date1.substring(9, 10));
		int days2 = Integer.parseInt(date2.substring(9, 10));
		//subtract years, months, days from each other. 
		int rYears = year1-year2;
		int rMonths = month1-month2;
		int rDays = days1-days2;
		//if days1 is less than days2, subtract 1 from month. If month1 is less than month2,
		//subtract 1 from years.
		if(rDays < 0){
			rMonths--;
		}
		if(rMonths < 0){
			rYears--;
		}
		
		//convert back into String.
		resOut.value = String.format("%s", rYears);
		resOut.resultType = Token.INTEGER;
		return resOut;
	}
}
