package utility;

import parser.Parser;
import parser.ResultValue;
import token.Token;

public final class Utility 
{
	
	/**
	 * Helper static routine to that validates an inputed date.
	 * @param parser
	 * @param date
	 * @return
	 */
	public static ResultValue validateDate( Parser parser, ResultValue resDate) throws Exception
	{
		ResultValue resOut = new ResultValue();
		resOut.value = "F";
		resOut.resultType = Token.BOOLEAN;

		int[] iDaysPerMonth = { 0, 31, 29, 31
	           					, 30, 31, 30
	           					, 31, 31, 30
	           					, 31, 30, 31 };
		int iCharCnt = 0;
		int year = 0;
		int month = 0;
		int days = 0;
		String date = resDate.value;
		//separate and convert dates into years, months, and days.
		try 
		{
			year = Integer.parseInt(date.substring(0, 4));
			month = Integer.parseInt(date.substring(5, 7));
			days = Integer.parseInt(date.substring(8, resDate.value.length()));
		}
		catch( Exception e)
		{
			parser.error("Invalid date value, found %s", resDate.value);
		}
		//System.out.println(" in validateDate() Year is " + year);
		//System.out.println(" in validateDate() Month is " + month);
		//System.out.println(" in validateDate() Day is " + days);
		// validate year
		
		// validate month
		if( month < 1 || month > 12 )
			parser.error("Invalid month for date \'%s\'", resDate.value);
		
		// validate day based on max days per month
		if( days < 1 || days > iDaysPerMonth[month])
			parser.error("Invalid days for date \'%s\'", resDate.value);
		
		resOut.value = "T";

		// if the 29th of Feb, check for leap year
	    if (days == 29 && month == 2)
	    {
	        if (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0))
	        {
	            //return 0;    // it is a leap year
	    		resOut.value = "T";
	        }
	        else
	        {
	        	resOut.value = "F";
	        	parser.error("Invalid day, not a leap year for date %s"
	        			, resDate.value);
	        }
	    }
	    
		return resOut;
	}
	
	
	/**
	 * Function that receives a ResultValue operand that should be String with a value
	 * and returns a ResultValue that contains the count of the number of chars in the
	 * given operand value as an int type.
	 * @param parser  The parser to delegate errors back
	 * @param resOperand The ResultValue thats one of four types with a value string literal
	 * @return A ResultValue containing numbers of chars counted as Integer type.
	 * @throws Exception 
	 */
	
	public static ResultValue inFunction(Parser parser, ResultValue value, ResultValue valueList) throws Exception{
		// throw an error if the second argument is not an array.
		if(valueList.arraySize < 1){
			parser.error("Second argument must be an array");
		}
		// throw an error if the first argument -is- an array
		if(! (value.resultType == 5)){
			if(value.arraySize > 0)
				parser.error("First argument can not be an array");
		}
		
		int i = 0;
		
		ResultValue resOut = new ResultValue();
		
		// Check Ints and Floats
		if(value.resultType == 2){
			int cmp1 = Integer.parseInt(value.value);
			int cmp2;
			for(i = 0; i < valueList.arraySize; i++){
				//System.out.println(valueList.valueList.get(i).value);
				if(valueList.resultType == 3)
					cmp2 = (int) Double.parseDouble(valueList.valueList.get(i).value);
				else cmp2 = Integer.parseInt(valueList.valueList.get(i).value);
				if(cmp1 == cmp2){
					// break to confirm that the value has been found
					break;
				}
			}	
		} else if (value.resultType == 3){
			double cmp1 = Double.parseDouble(value.value);
			double cmp2;
			for(i = 0; i < valueList.arraySize; i++){
				//System.out.println(valueList.valueList.get(i).value);
				if(valueList.resultType == 2)
					cmp2 = (double) Integer.parseInt(valueList.valueList.get(i).value);
				else cmp2 = Double.parseDouble(valueList.valueList.get(i).value);
				
				if(cmp1 == cmp2){
					// break to confirm that the value has been found
					break;
				}
			}	
		}
		// Check Strings
		else {
			String[] compare = valueList.value.split(","); // for string comparison
			for(i = 0; i < compare.length; i++){
				//System.out.println(valueList.valueList.get(i).value);
				if(value.value.equals(compare)){
					// break to confirm that the value has been found
					break;
				}
			}
		}
		
		// i will be equal to the array size if the value was never found
		//System.out.println("i: " + i);
		if(i == valueList.arraySize){
			resOut.value = String.format("F");
		}else resOut.value = String.format("T");
		
		return resOut;
	}
	
	public static ResultValue notInFunction(Parser parser, ResultValue value, ResultValue valueList) throws Exception{
		// throw an error if the second argument is not an array.
		if(valueList.arraySize < 1){
			parser.error("Second argument must be an array");
		}
		// throw an error if the first argument -is- an array
		if(! (value.resultType == 5)){
			if(value.arraySize > 0)
				parser.error("First argument can not be an array");
		}
				
		int i = 0;
				
		ResultValue resOut = new ResultValue();
				
		// Check Ints and Floats
		if(value.resultType == 2){
			int cmp1 = Integer.parseInt(value.value);
			int cmp2;
			for(i = 0; i < valueList.arraySize; i++){
				//System.out.println(valueList.valueList.get(i).value);
				if(valueList.resultType == 3)
					cmp2 = (int) Double.parseDouble(valueList.valueList.get(i).value);
				else cmp2 = Integer.parseInt(valueList.valueList.get(i).value);
				if(cmp1 == cmp2){
					// break to confirm that the value has been found
					break;
				}
			}	
		} else if (value.resultType == 3){
			double cmp1 = Double.parseDouble(value.value);
			double cmp2;
			for(i = 0; i < valueList.arraySize; i++){
				//System.out.println(valueList.valueList.get(i).value);
				if(valueList.resultType == 2)
					cmp2 = (double) Integer.parseInt(valueList.valueList.get(i).value);
				else cmp2 = Double.parseDouble(valueList.valueList.get(i).value);
				if(cmp1 == cmp2){
					// break to confirm that the value has been found
					break;
				}
			}	
		}
		// Check Strings
		else {
			String[] compare = valueList.value.split(","); // for string comparison
			for(i = 0; i < compare.length; i++){
				//System.out.println(valueList.valueList.get(i).value);
				if(value.value.equals(compare)){
					// break to confirm that the value has been found
					break;
				}
			}
		}
				
		// i will be equal to the array size if the value was never found
		if(i < valueList.arraySize){
			resOut.value = String.format("F");
		}else resOut.value = String.format("T");
				
		return resOut;
	}
	public static ResultValue length(Parser parser
			, ResultValue resStrOperand ) throws Exception
	{
		ResultValue resOut = new ResultValue();
		int num = 0;
		

		
		// check if its String type first
		if( resStrOperand.resultType == Token.STRING || resStrOperand.resultType == Token.BOOLEAN)
		{
			// do string char counting
			String str = resStrOperand.value;
			num = str.length();
		}
		else 
		{ 
			parser.error("Expected String input for LENGTH function, found " + resStrOperand.resultType);
		}
		
		// set resOut value and type
		resOut.resultType = 2;
		resOut.value = String.format("%s", num);
		return resOut;
	}
	
	/**
	 * Function that receives a ResultValue operand that should be String type with a value
	 * and returns a ResultValue that contains a boolean type value indicating T if the string literal
	 * is empty or only contains spaces, F otherwise.
	 * @param parser  The parser to delegate errors back
	 * @param resStrOperand The ResultValue thats String type with a value string literal
	 * @return A ResultValue of boolean type with value T or F
	 * @throws Exception 
	 */
	public static ResultValue spaces(Parser parser
			, ResultValue resStrOperand ) throws Exception
	{
		ResultValue resOut = new ResultValue();
		

		
		// check if its String type first
		if( resStrOperand.resultType == Token.STRING ){
		
		// do string check for any characters present
			String str = resStrOperand.value;
			char[] chr = str.toCharArray();
			int i = 0;
			while(i < str.length())
			{
				if(chr[i] != ' ' && chr[i] != '\t' && chr[i] != '\n')
				{
					resOut.resultType = 4;
					resOut.value = "F";
					return resOut;				
				}
				i++;
			}
		} else {
			parser.error("Expected String, found " + resStrOperand.resultType);
		}
		
		// set resOut value and type
		resOut.resultType = 4;
		resOut.value = "T";
		return resOut;
	}
	
	/**
	 * Returns subscript of highest populated element of the array + 1
	 * @param parser
	 * @param resStrOperand
	 * @return
	 * @throws Exception
	 */
	public static ResultValue elem(Parser parser, 
			ResultValue resOperand ) throws Exception
	{
		ResultValue resOut = new ResultValue();
		int num = 0;
		int i = 0;
		
		if( resOperand.resultType == Token.INTEGER 
				|| resOperand.resultType == Token.FLOAT 
				|| resOperand.resultType == Token.BOOLEAN  )
		{
			for( ResultValue element : resOperand.valueList )
			{
				if(  !element.value.equals("null"))
				{
					//System.out.printf(" in elem():  valueList[%s]=%s\n", i, element.value);

					num += 1;
				}
				i++;					
			}
		} 
		else
		{
			parser.errorWithCurrent("Only allowed Int, Float, or Boolean array for ELEM parameter");
		}
		
		resOut.resultType = Token.INTEGER;
		resOut.value = String.format("%d", num);
		
		return resOut;
	}
	
	/**
	 * Returns the declared size for a given aray type variable
	 * @param parser
	 * @param resStrOperand
	 * @return
	 * @throws Exception
	 */
	public static ResultValue maxElem(Parser parser, 
			ResultValue resOperand ) throws Exception
	{
		ResultValue resOut = new ResultValue();
		int num = 0;
		
		if( resOperand.resultType == Token.INTEGER 
				|| resOperand.resultType == Token.FLOAT 
				|| resOperand.resultType == Token.BOOLEAN  )
		{
			num = resOperand.arraySize;
		}
		else
		{
			parser.errorWithCurrent("Only allowed Int, Float, or Boolean array for ELEM parameter");
		}
		
		resOut.resultType = Token.INTEGER;
		resOut.value = String.format("%d", num);
	
		return resOut;
	}
	
	public static ResultValue exponent(Parser parser
			    , ResultValue resOp1
			    , ResultValue resOp2 ) throws Exception
	{
		ResultValue resOut = new ResultValue(); 
		
		// convert input ResultValues to Numerics
		Numeric op1 = new Numeric( parser, resOp1, "^", "1st operand");
		Numeric op2 = new Numeric( parser, resOp2, "^", "2nd operand");
		
		if( resOp1.resultType == Token.INTEGER )
		{
			int num1 = op1.getInt();
			int num2 = (int)op2.getFloat();
			resOut.resultType = Token.INTEGER;
			//System.out.println("inside Utility.exponent() " + "op1: " + num1 + " op2: " + num2 + " Result: " + (int)Math.pow(num1, num2));
			resOut.value = String.format( "%d", (int)Math.pow(num1, num2));
		}
		else if( resOp1.resultType == Token.FLOAT )

		{
			double num1 = op1.getFloat();
			double num2 = op2.getFloat();
			resOut.resultType = Token.FLOAT;
			resOut.value = String.format("%.2lf", Math.pow(num1, num2));

		}
		else
		{
			parser.error("Expected numeric type for operations");

		}
		return resOut;
	}
	
	public static ResultValue multiply(Parser parser
		    , ResultValue resOp1
		    , ResultValue resOp2 ) throws Exception
	{
		ResultValue resOut = new ResultValue(); 
		
		// convert input ResultValues to Numerics
		Numeric op1 = new Numeric( parser, resOp1, "*", "1st operand");
		Numeric op2 = new Numeric( parser, resOp2, "*", "2nd operand");
		//System.out.printf("\t\tDEBUG: Inside Utility.multiply(): op1=%s op2=%s\n", resOp1.value, resOp2.value);
		//System.out.printf("\t\tDEBUG: Inside Utility.multiply(): resultType of op1 = %s\n", resOp1.resultType);
	
		// perform math operations
		if( resOp1.resultType == Token.INTEGER )
		{
			int num1 = op1.getInt();
			int num2 = (int)op2.getFloat();
			resOut.resultType = Token.INTEGER;
			resOut.value = String.format( "%d", ( num1 * num2 ));
		}
		else if(resOp1.resultType == Token.FLOAT )
		{
			double num1 = op1.getFloat();
			double num2 = op2.getFloat();
			resOut.resultType = Token.FLOAT;
			resOut.value = String.format("%.2f", (double)num1 * num2);
		}
		else
		{
			parser.errorWithCurrent("Expected numeric type for operations");
		}
		return resOut;
	}
	public static ResultValue divide (Parser parser
			, ResultValue resOp1
			, ResultValue resOp2) throws Exception
	{
		ResultValue resOut = new ResultValue(); 
		
		// convert input ResultValues to Numerics
		Numeric op1 = new Numeric( parser, resOp1, "/", "1st operand");
		Numeric op2 = new Numeric( parser, resOp2, "/", "2nd operand");
		//System.out.printf("\t\tDEBUG: Inside Utility.multiply(): op1=%s op2=%s\n", resOp1.value, resOp2.value);
		//System.out.printf("\t\tDEBUG: Inside Utility.multiply(): resultType of op1 = %s\n", resOp1.resultType);
	
		// perform math operations
		if( resOp1.resultType == Token.INTEGER )
		{
			int num1 = op1.getInt();
			int num2 = (int)op2.getFloat();
			resOut.resultType = Token.INTEGER;
			resOut.value = String.format( "%d", ( num1 / num2 ));
		}
		else if(resOp1.resultType == Token.FLOAT )
		{
			double num1 = op1.getFloat();
			double num2 = op2.getFloat();
			resOut.resultType = Token.FLOAT;
			resOut.value = String.format("%.2f", (double)num1 / num2);
		}
		else
		{
			parser.errorWithCurrent("Expected numeric type for operations");
		}
		return resOut;
	}
	/*
	 * Addition method
	 * TODO Need to make variable number of arguments, check Parsing Part 3 Notes
	 */
	public static ResultValue add(Parser parser, ResultValue resOp1, ResultValue resOp2) throws Exception
	{
		ResultValue resOut = new ResultValue();

		Numeric op1 = new Numeric(parser, resOp1, "+", "1st operand");
		Numeric op2 = new Numeric(parser, resOp2, "+", "2nd operand");
		
		//System.out.printf("\t\tDEBUG: Inside Utility.add(): op1=%s op2=%s\n", resOp1.value, resOp2.value);
		//System.out.printf("\t\tDEBUG: Inside Utility.add(): resultType of op1 = %s\n", resOp1.resultType);

		
		if(resOp1.resultType == Token.INTEGER)
		{
			int num1 = op1.getInt();
			int num2 = (int)op2.getFloat();
			resOut.resultType = Token.INTEGER;
			resOut.value = String.format("%d", (num1 + num2));
		} 
		else if (resOp1.resultType == Token.FLOAT)
		{
			double num1 = op1.getFloat();
			double num2 = op2.getFloat();
			resOut.resultType = Token.FLOAT;
			resOut.value = String.format("%.2f", (num1 + num2));
		} 
		else parser.errorWithCurrent("Expected numeric type for operations");
		
		//System.out.printf("\t\tDEBUG: Inside Utility.add(): return val is %s\n", resOut.value );

		return resOut;
	}
	/*
	 * For subtraction
	 */
	public static ResultValue minus(Parser parser, ResultValue resOp1, ResultValue resOp2) throws Exception{
		Numeric op1 = new Numeric(parser, resOp1, "-", "1st operand");
		Numeric op2 = new Numeric(parser, resOp2, "-", "2nd operand");
		ResultValue resOut = new ResultValue();
		
		if(resOp1.resultType == Token.INTEGER){
			int num1 = op1.getInt();
			int num2 = (int)op2.getFloat();
			resOut.resultType = Token.INTEGER;
			resOut.value = String.format("%d", (num1 - num2));
		} else if (resOp1.resultType == Token.FLOAT){
			double num1 = op1.getFloat();
			double num2 = op2.getFloat();
			resOut.resultType = Token.FLOAT;
			resOut.value = String.format("%.2f", (num1 - num2));
		} else parser.error("Expected numeric type for operations");
		
		return resOut;
	}
	/*
	 * For negative numbers
	 */
	public static ResultValue minus(Parser parser, ResultValue resOp) throws Exception{
		Numeric op1 = new Numeric(parser, resOp, "-", "1st operand");
		ResultValue resOut = new ResultValue();
		
		if(resOp.resultType == Token.INTEGER){
			int num1 = op1.getInt();
			int num2 = 0;
			resOut.resultType = Token.INTEGER;
			resOut.value = String.format("%d", (num2 - num1));
		} else if (resOp.resultType == Token.FLOAT){
			double num1 = op1.getFloat();
			double num2 = 0.0;
			resOut.resultType = Token.FLOAT;
			resOut.value = String.format("%.2f", (num2 - num1));
		} else parser.error("Expected numeric type for operations");
		
		return resOut;
	}
	
	/*
	 * Greater than comparison
	 */
	public static ResultValue greaterThan(Parser parser, ResultValue resOp1, ResultValue resOp2) throws Exception
	{
		Numeric op1 = new Numeric(parser, resOp1, ">", "1st operand");
		Numeric op2 = new Numeric(parser, resOp2, ">", "2nd operand");
		ResultValue resOut = new ResultValue();
		
		if(resOp1.resultType == Token.INTEGER){
			int num1 = op1.getInt();
			int num2 = (int)op2.getFloat();
			resOut.resultType = Token.BOOLEAN;
			if(num1 > num2)
				resOut.value = String.format("T");
			else resOut.value = String.format("F");
			
		} else if (resOp1.resultType == Token.FLOAT){
			double num1 = op1.getFloat();
			double num2 = op2.getFloat();
			resOut.resultType = Token.BOOLEAN;
			if(num1 > num2)
				resOut.value = String.format("T");
			else resOut.value = String.format("F");
			
		} else if (resOp1.resultType == Token.STRING){
			String num1 = resOp1.value;
			String num2 = resOp2.value;
			resOut.resultType = Token.BOOLEAN;
			if((num1.compareTo(num2)) > 0)
				resOut.value = String.format("T");
			else resOut.value = String.format("F");
			
		} else parser.error("Expected numeric or string type for operations");
		
		
		return resOut;
	}
	
	/*
	 * Less than comparison
	 */
	public static ResultValue lessThan(Parser parser, ResultValue resOp1, ResultValue resOp2) throws Exception
	{
		Numeric op1 = new Numeric(parser, resOp1, "<", "1st operand");
		Numeric op2 = new Numeric(parser, resOp2, "<", "2nd operand");
		ResultValue resOut = new ResultValue();
		
		if(resOp1.resultType == Token.INTEGER)
		{
			int num1 = op1.getInt();
			int num2 = (int)op2.getFloat();
			resOut.resultType = Token.BOOLEAN;
			if(num1 < num2)
				resOut.value = String.format("T");
			else resOut.value = String.format("F");
			
		}
		else if (resOp1.resultType == Token.FLOAT)
		{
			double num1 = op1.getFloat();
			double num2 = op2.getFloat();
			resOut.resultType = Token.BOOLEAN;
			if(num1 < num2)
				resOut.value = String.format("T");
			else resOut.value = String.format("F");
			
		} 
		else if (resOp1.resultType == Token.STRING)
		{
			String num1 = resOp1.value;
			String num2 = resOp2.value;
			resOut.resultType = Token.BOOLEAN;
			if((num1.compareTo(num2)) < 0)
				resOut.value = String.format("T");
			else resOut.value = String.format("F");
			
		} else parser.error("Expected numeric or string type for operations");
		
		
		return resOut;
	}
	
	/*
	 * Greater than or equal to comparison
	 */
	public static ResultValue greaterThanEqual(Parser parser, ResultValue resOp1, ResultValue resOp2) throws Exception{
		Numeric op1 = new Numeric(parser, resOp1, ">=", "1st operand");
		Numeric op2 = new Numeric(parser, resOp2, ">=", "2nd operand");
		ResultValue resOut = new ResultValue();
		
		if(resOp1.resultType == Token.INTEGER){
			int num1 = op1.getInt();
			int num2 = (int)op2.getFloat();
			resOut.resultType = Token.BOOLEAN;
			if(num1 >= num2)
				resOut.value = String.format("T");
			else resOut.value = String.format("F");
			
		} else if (resOp1.resultType == Token.FLOAT){
			double num1 = op1.getFloat();
			double num2 = op2.getFloat();
			resOut.resultType = Token.BOOLEAN;
			if(num1 >= num2)
				resOut.value = String.format("T");
			else resOut.value = String.format("F");
			
		} else if (resOp1.resultType == Token.STRING){
			String num1 = resOp1.value;
			String num2 = resOp2.value;
			resOut.resultType = Token.BOOLEAN;
			if((num1.compareTo(num2)) >= 0)
				resOut.value = String.format("T");
			else resOut.value = String.format("F");
			
		} else parser.error("Expected numeric or string type for operations");
		
		
		return resOut;
	}
	
	/*
	 * Less than or equal to comparison
	 */
	public static ResultValue lessThanEqual(Parser parser, ResultValue resOp1, ResultValue resOp2) throws Exception{
		Numeric op1 = new Numeric(parser, resOp1, "<=", "1st operand");
		Numeric op2 = new Numeric(parser, resOp2, "<=", "2nd operand");
		ResultValue resOut = new ResultValue();
		
		if(resOp1.resultType == Token.INTEGER)
		{
			int num1 = op1.getInt();
			int num2 = (int)op2.getFloat();
			resOut.resultType = Token.BOOLEAN;
			if(num1 <= num2)
				resOut.value = String.format("T");
			else resOut.value = String.format("F");
			
		} else if (resOp1.resultType == Token.FLOAT)
		{
			double num1 = op1.getFloat();
			double num2 = op2.getFloat();
			resOut.resultType = Token.BOOLEAN;
			if(num1 <= num2)
				resOut.value = String.format("T");
			else resOut.value = String.format("F");
			
		} 
		else if (resOp1.resultType == Token.STRING)
		{
			String num1 = resOp1.value;
			String num2 = resOp2.value;
			resOut.resultType = Token.BOOLEAN;
			if((num1.compareTo(num2)) <= 0)
				resOut.value = String.format("T");
			else resOut.value = String.format("F");
			
		}
		else parser.error("Expected numeric or string type for operations");
		return resOut;
	}
	
	/*
	 * Not equal to comparison
	 */
	public static ResultValue notEqual(Parser parser, ResultValue resOp1, ResultValue resOp2) throws Exception
	{
		Numeric op1 = new Numeric(parser, resOp1, "!=", "1st operand");
		Numeric op2 = new Numeric(parser, resOp2, "!=", "2nd operand");
		ResultValue resOut = new ResultValue();
		
		if(resOp1.resultType == Token.INTEGER)
		{
			int num1 = op1.getInt();
			int num2 = (int)op2.getFloat();
			resOut.resultType = Token.BOOLEAN;
			if(num1 != num2)
				resOut.value = String.format("T");
			else resOut.value = String.format("F");
			
		}
		else if (resOp1.resultType == Token.FLOAT)
		{
			double num1 = op1.getFloat();
			double num2 = op2.getFloat();
			resOut.resultType = Token.BOOLEAN;
			if(num1 != num2)
				resOut.value = String.format("T");
			else resOut.value = String.format("F");
			
		}
		else if (resOp1.resultType == Token.STRING)
		{
			String num1 = resOp1.value;
			String num2 = resOp2.value;
			resOut.resultType = Token.BOOLEAN;
			if((num1.compareTo(num2)) != 0)
				resOut.value = String.format("T");
			else resOut.value = String.format("F");
			
		} 
		else parser.error("Expected numeric or string type for operations");
		

		
		return resOut;
	}
	

	public static ResultValue equal(Parser parser, ResultValue resOp1, ResultValue resOp2 ) throws Exception
	{
		Numeric op1 = new Numeric(parser, resOp1, "==", "1st operand");
		Numeric op2 = new Numeric(parser, resOp2, "==", "2nd operand");
		ResultValue resOut = new ResultValue();
		
		if(resOp1.resultType == Token.INTEGER)
		{
			int num1 = op1.getInt();
			int num2 = (int)op2.getFloat();
			resOut.resultType = Token.BOOLEAN;
			if(num1 == num2)
				resOut.value = String.format("T");
			else resOut.value = String.format("F");
			
		} 
		else if (resOp1.resultType == Token.FLOAT) 
		{
			double num1 = op1.getFloat();
			double num2 = op2.getFloat();
			resOut.resultType = Token.BOOLEAN;
			if(num1 == num2)
				resOut.value = String.format("T");
			else resOut.value = String.format("F");
		}
		else if (resOp1.resultType == Token.STRING)
		{
			String num1 = resOp1.value;
			String num2 = resOp2.value;
			resOut.resultType = Token.BOOLEAN;
			if((num1.compareTo(num2)) == 0)
				resOut.value = String.format("T");
			else resOut.value = String.format("F");
		}
		else parser.error("Expected numeric or string type for operations");
		
		//System.out.printf("\t\tDEBUG: Inside == equal(): Cond eval to %s\n", resOut.value );

		
		return resOut;
	}
	
	/**
	 * Returns the boolean T if both operands are true and returns F otherwise. The operands
	 * must be either a boolean constant or are boolean expressions.
	 * @param parser
	 * @param resOp1
	 * @param resOp2
	 * @return
	 * @throws Exception
	 */
	public static ResultValue and(Parser parser, ResultValue resOp1, ResultValue resOp2 ) throws Exception
	{
		//Numeric op1 = new Numeric(parser, resOp1, "and", "1st operand");
		//Numeric op2 = new Numeric(parser, resOp2, "and", "2nd operand");
		ResultValue resOut = new ResultValue();
		
		if(resOp1.resultType == Token.BOOLEAN && resOp2.resultType == Token.BOOLEAN )
		{
			
			resOut.resultType = Token.BOOLEAN;
			if( resOp1.value.equals(resOp2.value)  )
				resOut.value = String.format("T");
			else resOut.value = String.format("F");
			
		} 
		else parser.error("Expected boolean constant or boolean expression for logical \'and\'");
		
		//System.out.printf("\t\tDEBUG: Inside == equal(): Cond eval to %s\n", resOut.value );

		
		return resOut;
	}
	
	public static ResultValue or(Parser parser, ResultValue resOp1, ResultValue resOp2 ) throws Exception
	{

		ResultValue resOut = new ResultValue();
		
		if(resOp1.resultType == Token.BOOLEAN && resOp2.resultType == Token.BOOLEAN )
		{
			
			resOut.resultType = Token.BOOLEAN;
			if( resOp1.value.equals("T") || resOp2.value.equals("T")  )
				resOut.value = String.format("T");
			else resOut.value = String.format("F");
			
		} 
		else parser.error("Expected boolean constant or boolean expression for logical \'and\'");
		
		//System.out.printf("\t\tDEBUG: Inside == equal(): Cond eval to %s\n", resOut.value );
		
		return resOut;
	}
	
	public static ResultValue not(Parser parser, ResultValue resOperand) throws Exception
	{
		ResultValue resOut = new ResultValue();
		
		resOut.resultType = Token.BOOLEAN;
		
		if( resOperand.resultType == Token.BOOLEAN )
		{
			if( resOperand.value.equals("F") )
			{
				resOut.value = "T";
			}
			else
			{
				resOut.value = "F";
			}
		}
		else
		{
			parser.errorWithCurrent("Expected BOOLEAN type for \'not\'");
		}
		
		return resOut;
	}
	
	/**
	 * 
	 * @param parser A reference to the Parser
	 * @param value
	 * @return
	 * @throws Exception
	 */
	public static String formatStringValue( Parser parser, String value ) throws Exception
	{
		char [] chArray = value.toCharArray();
		String tokenStrTemp = "";
		
		for( int i = 0; i < chArray.length; i++ )
		{
			// if we found the start of an escaped character
			if( "\\".indexOf( chArray[i] ) > -1  )
			{
				i++;
				//System.out.println("Found escaped at " + i);
				
				// check the next char byte for substitution
				if( "\"".indexOf(chArray[i]) > -1 ) // double quote
				{
					tokenStrTemp += String.valueOf(  chArray[i]);
				}
				else if( "\'".indexOf(chArray[i]) > -1 ) // apostrophe, single quote
				{
					tokenStrTemp += String.valueOf(  chArray[i] );
				}
				else if( "\\".indexOf(chArray[i]) > -1 ) // escaped back slash
				{
					tokenStrTemp += String.valueOf(  chArray[i] );
				} 
				else if( "t".indexOf(chArray[i]) > -1 ) // tab
				{
					//System.out.println("Replace '\\t' at " + i);

					//chArray[i] = 0x09;
					tokenStrTemp += "    ";
				}
				else if( "n".indexOf(chArray[i]) > -1 ) // line feed
				{
					//System.out.println("Replace '\\n' at " + i)
					//chArray[i] = 0x0A;
					tokenStrTemp += "\n";
				}
				
				else if( "a".indexOf(chArray[i]) > -1 ) // alarm bell
				{
					chArray[i] = 0x07;
					tokenStrTemp += String.valueOf(  chArray[i] );
				}
				else // unrecognized escape char?
				{
					parser.error(": Invalid escaped character \'" + chArray[i-1] + chArray[i] + "\' " );
				}
			}
			else // add the char back into temp string array
			{
				tokenStrTemp += String.valueOf( chArray[i] );
			}
			//System.out.println(  tokenStr );
		}
		return tokenStrTemp;
	}

	public static ResultValue concatenate(Parser parser
			, ResultValue resOp1
			, ResultValue resOp2) throws Exception
	{
		ResultValue op1 = resOp1;
		ResultValue op2 = resOp2;
		ResultValue resOut = new ResultValue();
		resOut.resultType = Token.STRING;
		
		// Coerce the operands
		if( resOp1.resultType != Token.STRING )
		{
			op1 = coerce( parser, resOp1 );
		}
		if( resOp2.resultType != Token.STRING )
		{
			op2 = coerce( parser, resOp2 );
		}
			
		// quick error check before executing
		if( op1.resultType != Token.STRING && op2.resultType != Token.STRING)
			parser.error("Concatenation arguments are not STRING, INTEGER, or FLOAT type");
		
		// concatenate the supposed Strings
		StringBuilder builder = new StringBuilder();
		resOut.value = builder.append(op1.value).append(op2.value).toString();
		resOut.arraySize = builder.toString().length();
		
		for( int i = 0; i<resOut.arraySize; i++ )
		{
			ResultValue iChar = new ResultValue();
			iChar.value = String.format("%c", builder.toString().charAt(i));
			resOut.valueList.add(iChar);
		}
		
		
		return resOut;
	}
	
	/**
	 * Converts a given Integer or Float to  String type value
	 * @param parser
	 * @param resOperand
	 * @return ResultValue that coerced that inout Int or Double to a String value
	 * @throws Exception
	 */
	public static ResultValue coerce(Parser parser, ResultValue resOperand ) throws Exception
	{
		ResultValue returnRes = new ResultValue();
		String val = "";

		if( resOperand.resultType == Token.INTEGER )
		{
			val = String.format("%s", resOperand.value);
		}
		else if( resOperand.resultType == Token.FLOAT )
		{
			val = String.format("%.2lf", resOperand.value);
		}
		
		returnRes.value = val;
		returnRes.resultType = Token.STRING;
		return returnRes;
	}

	/**
	 * Converts a given operand to an Integer type ResultValue.
	 * @param parser
	 * @param resOperand
	 * @return
	 * @throws Exception
	 */
	public static ResultValue toInteger( Parser parser, ResultValue resOperand ) throws Exception
	{
		ResultValue resOut = new ResultValue();
		int num;
		
		// try to parse int for this thing
		if( resOperand.resultType == Token.STRING )
		{
			try 
			{
				num = Integer.parseInt( resOperand.value );
				resOut.value = String.format("%d", num);
			} 
			catch (Exception e)
			{
				parser.errorWithCurrent("String value for integer contains characters");
			}
		}
		else if( resOperand.resultType == Token.FLOAT )
		{
			try
			{
				num = (int) Double.parseDouble(resOperand.value);
				resOut.value = String.format("%d", num);
			}
			catch (Exception e)
			{
				parser.errorWithCurrent("Unable to convert float to integer");
			}
		}
		else if (resOperand.resultType == Token.INTEGER  ) //
		{
			resOut = resOperand.clone();
		}
		else
		{
			parser.errorWithCurrent("Invalid datatype for coercing to Integer");

		}
		resOut.resultType = Token.INTEGER;

		return resOut;
	}

	/**
	 * Returns a boolean specifying T if the boolean operand condition is F, otherwise False.
	 * A negation.
	 * @param parser
	 * @param resOperand
	 * @return
	 */

}
