package utility;

import java.util.HashMap;

import parser.Parser;
import parser.ResultValue;

public class StorageManager 
{
	private HashMap<String, ResultValue> varTable;
	
	/**
	 * Initializes a new StorageManager object with an empty HashMap.
	 */
	public StorageManager() 
	{
		varTable = new HashMap<String, ResultValue>();
	}
	
	

	/**
	 * Assigns the variable its ResultValue and saves it into the StorageManager.
	 * @param variableStr The identifier of th variable.
	 * @param result The ResultValue of this variable
	 */
	public ResultValue assign( String variableStr, ResultValue result )
	{
		varTable.put(variableStr, result);
		return (ResultValue) varTable.get(variableStr);
	}
	
	/**
	 * Retrieves the ResultValue for the given variable name. Uses Parser for error handling
	 * @param parser Reference to the Parser.
	 * @param variableStr The string key into the StorageManager Hash Table identifying the variable.
	 * @return The retrieved ResultValue for the given variable
	 * @throws Exception The variable does not exist in the StorageManager.
	 */
	public ResultValue getVariableValue(Parser parser, String variableStr) throws Exception
	{
		if (varTable.get(variableStr) == null )
			parser.error("Variable \'%s\' has not been declared", variableStr);
		return (ResultValue) varTable.get(variableStr);
	}

}
