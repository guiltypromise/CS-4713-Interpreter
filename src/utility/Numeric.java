package utility;

import parser.Parser;
import parser.ResultValue;

public class Numeric 
{
	Parser parser;
	ResultValue res;
	String operator;
	String operatorOrder;
	
	/**
	 * Constructs a new Numeric operand that takes in a reference to the Parser
	 * for error handling, a reference to the ResultValue for this operand, the operator
	 * involving the operand, and an identifier string indicating if its the first or second operand.
	 * @param parserObj
	 * @param res
	 * @param operator
	 * @param operatorOrder
	 */
	public Numeric( Parser parserObj
			, ResultValue res
			, String operator
			, String operatorOrder) 
	{
		// TODO Auto-generated constructor stub
		this.parser = parserObj;
		this.res = res;
		this.operator = operator;
		this.operatorOrder = operatorOrder;
	}
	
	/**
	 * When called, returns the integer value of the ResultValue.
	 * @return int res.value 
	 * @throws Exception 
	 */
	public int getInt() throws Exception 
	{
		ResultValue resOut = new ResultValue();
		try{
			return Integer.parseInt(res.value);	
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("must be an integer");
		}
	}
	
	/**
	 * When called, returns the double value of the ResultValue.
	 * @return double res.value
	 * @throws Exception 
	 */
	public double getFloat() throws Exception {
		try{
			return Double.parseDouble(res.value);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("must be a double");
		}
		
	}



}
