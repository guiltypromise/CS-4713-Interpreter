package symboltable;

public class STControl extends STEntry{

	public int subClassif; // subClassification(flow(10), end(11), declare(12))
	
	/**
	 * Constructor that classifies control tokens into their subClassif, 
	 * the secondary classification under the primary classification (control).
	 * 
	 * @param symbol
	 * @param primClasif
	 * @param subClassif
	 */
	public STControl(String symbol, int primClasif, int subClassif) {
		super(symbol, primClasif);
		this.subClassif = subClassif;
	}

}
