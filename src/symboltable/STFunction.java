package symboltable;

import java.util.ArrayList;

public class STFunction extends STEntry {
	
	public int returnType; // return data type (int(1), float(2), string(3), bool(4), date(5), void(6))
	public int definedBy; // what defined it(user(13), builtin(14))
	public int numArgs; // the number of arguments. For variable length, VAR_ARGS
	public ArrayList paramList; // reference to an ArrayList of formal parameters
	public SymbolTable symbolTable; // reference to the function's symbol table if it is user-defined
	
	/**
	 * Constructor that sets several parameters to local variables for later use.
	 * 
	 * @param symbol
	 * @param primClasif
	 * @param returnType
	 * @param definedBy
	 * @param numArgs
	 * @param paramList
	 * @param symbolTable
	 */
	public STFunction(String symbol, int primClasif, int returnType, int definedBy,
			int numArgs, ArrayList paramList, SymbolTable symbolTable) {
		super(symbol, primClasif);
		this.numArgs = numArgs;
		this.definedBy = definedBy;
		this.returnType = returnType;
		this.paramList = paramList;
		this.symbolTable = symbolTable; // null for builtin functions
	}
}
