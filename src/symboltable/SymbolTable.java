package symboltable;

import java.util.HashMap;

import token.Token;

public class SymbolTable 
{
	private HashMap<String, STEntry> globalTable;
	
	/**
	 * Constructor to initialize the initGlobal() global symbol table.
	 */
	public SymbolTable(){
		initGlobal();
	}
	
	/**
	 * Getter to access the symbol table, returning the Symbol Table Entry (STEntry)
	 * of the passed symbol.
	 * 
	 * @param symbol
	 * @return	the STEntry of the given symbol.
	 */
	public STEntry getSymbol(String symbol){
		return (STEntry) globalTable.get(symbol);
	}
		
	/**
	 * Method to associate the passed String symbol with the passed STEntry entry in the 
	 * initGlobal() Global Table.
	 * 
	 * @param symbol
	 * @param entry
	 */
	public void putSymbol(String symbol, STEntry entry){
		globalTable.put(symbol, entry);
	}
		
	/**
	 * Initializes the global symbol table which classifies certain strings as control tokens,
	 * function tokens, or operators with STControl(), STFunction(), STEntry(). 
	 */
	public void initGlobal(){
		globalTable = new HashMap<String, STEntry>();
		globalTable.put("def", new STControl("if", Token.CONTROL, Token.FLOW));
		globalTable.put("enddef", new STControl("endif", Token.CONTROL, Token.END));
		globalTable.put("if", new STControl("if", Token.CONTROL, Token.FLOW));
		globalTable.put("endif", new STControl("endif", Token.CONTROL, Token.END));
		globalTable.put("else", new STControl("else", Token.CONTROL, Token.END));
		globalTable.put("for", new STControl("for", Token.CONTROL, Token.FLOW));
		globalTable.put("endfor", new STControl("endfor", Token.CONTROL, Token.END));
		globalTable.put("while", new STControl("while", Token.CONTROL, Token.FLOW));
		globalTable.put("endwhile", new STControl("while", Token.CONTROL, Token.END));
		globalTable.put("select", new STControl("select", Token.CONTROL, Token.FLOW));
		globalTable.put("when", new STControl("when", Token.CONTROL, Token.END));
		globalTable.put("default", new STControl("default", Token.CONTROL, Token.END));
		globalTable.put("endselect", new STControl("endselect", Token.CONTROL, Token.END));
		globalTable.put("break", new STControl("break", Token.CONTROL, Token.END));
		globalTable.put("continue", new STControl("continue", Token.CONTROL, Token.END));
		globalTable.put("print", new STFunction("print", Token.FUNCTION, Token.VOID,
				Token.BUILTIN, 1, null, null));
		globalTable.put("Int", new STControl("Int", Token.CONTROL, Token.DECLARE));
		globalTable.put("Float", new STControl("Float", Token.CONTROL, Token.DECLARE));
		globalTable.put("String", new STControl("String", Token.CONTROL, Token.DECLARE));
		globalTable.put("Bool", new STControl("Bool", Token.CONTROL, Token.DECLARE));
		globalTable.put("Date", new STControl("Date", Token.CONTROL, Token.DECLARE));
		globalTable.put("array", new STFunction("array", Token.FUNCTION, Token.INTEGER,
				Token.BUILTIN, 2, null, null));
		globalTable.put("LENGTH", new STFunction("LENGTH", Token.FUNCTION, Token.INTEGER,
				Token.BUILTIN, 1, null, null));
		globalTable.put("MAXLENGTH", new STFunction("MAXLENGTH", Token.FUNCTION, Token.INTEGER,
				Token.BUILTIN, 1, null, null));
		globalTable.put("SPACES", new STFunction("SPACES", Token.FUNCTION, Token.BOOLEAN,
				Token.BUILTIN, 1, null, null));
		globalTable.put("ELEM", new STFunction("ELEM", Token.FUNCTION, Token.INTEGER,
				Token.BUILTIN, 1, null, null));
		globalTable.put("MAXELEM", new STFunction("MAXELEM", Token.FUNCTION, Token.INTEGER,
				Token.BUILTIN, 1, null, null));
		globalTable.put("dateDiff", new STFunction("dateDiff", Token.FUNCTION, Token.DATE,
				Token.BUILTIN, 1, null, null));
		globalTable.put("dateAge", new STFunction("dateAge", Token.FUNCTION, Token.DATE,
				Token.BUILTIN, 1, null, null));
		globalTable.put("dateAdj", new STFunction("dateAdj", Token.FUNCTION, Token.DATE,
				Token.BUILTIN, 1, null, null));
		globalTable.put("and", new STEntry("and", Token.OPERATOR));
		globalTable.put("or", new STEntry("or", Token.OPERATOR));
		globalTable.put("not", new STEntry("not", Token.OPERATOR));
		globalTable.put("IN", new STEntry("IN", Token.OPERATOR));
		globalTable.put("NOTIN", new STEntry("NOTIN", Token.OPERATOR));
	}
}
