package symboltable;

import java.util.ArrayList;

import parser.ResultValue;

public class STIdentifier extends STEntry implements Cloneable{
	

	public int dclType; //declaration type (Int (0), Float(1), String(2), Bool(3), Date(4))
	public int structure; // data structure (primitive(0), fixed array(1), unbound array(2))
	public int param; // parameter type (not a param, by reference, by value)
	public int nonLocal; // nonLocal base Address Ref (0-local, 1-surrounding, ..., k-surrounding, 99-global)
	/**
	 * Constructor that sets several variables for later use.
	 * 
	 * @param symbol
	 * @param primClasif
	 * @param dclType
	 * @param structure
	 * @param param
	 * @param nonLocal
	 */
	public STIdentifier(String symbol, int primClasif, int dclType, int structure,
			int param, int nonLocal) {
		super(symbol, primClasif);
		this.dclType = dclType;
		this.structure = structure;
		this.param = param;
		this.nonLocal = nonLocal;
	}
	
	public STIdentifier clone() throws CloneNotSupportedException
	{
		return (STIdentifier) super.clone();
	}
}
