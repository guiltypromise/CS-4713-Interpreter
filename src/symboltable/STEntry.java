package symboltable;

public class STEntry 
{
	public String symbol; // string for the symbol
	public int primClassif; // primary classification for the symbol
	
	/**
	 * Constructor that sets local String symbol and local int primClassif
	 * according to the passed values. The primClassif should be the int
	 * referring to the operator classification.
	 * 
	 * @param symbol
	 * @param primClassif
	 */
	public STEntry(String symbol, int primClassif){
		this.symbol = symbol;
		this.primClassif = primClassif;
	}

}
